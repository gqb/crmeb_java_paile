package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.ActivityLog;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.ActivityLogRequest;
import com.zbkj.common.request.course.ActivityLogSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.ActivityLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/activity-log")
@Api(tags = "活动签到日志") //配合swagger使用

public class ActivityLogController {

    @Autowired
    private ActivityLogService activityLogService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<ActivityLog>>  getList(@Validated ActivityLogSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<ActivityLog> activityLogCommonPage = CommonPage.restPage(activityLogService.getList(request, pageParamRequest));
        return CommonResult.success(activityLogCommonPage);
    }

    /**
     * 新增
     * @param activityLogRequest 新增参数
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@Validated ActivityLogRequest activityLogRequest){
        ActivityLog activityLog = new ActivityLog();
        BeanUtils.copyProperties(activityLogRequest, activityLog);

        if(activityLogService.save(activityLog)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(activityLogService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param id integer id
     * @param activityLogRequest 修改参数
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestParam Integer id, @Validated ActivityLogRequest activityLogRequest){
        ActivityLog activityLog = new ActivityLog();
        BeanUtils.copyProperties(activityLogRequest, activityLog);
        activityLog.setId(id);

        if(activityLogService.updateById(activityLog)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<ActivityLog> info(@RequestParam(value = "id") Integer id){
        ActivityLog activityLog = activityLogService.getById(id);
        return CommonResult.success(activityLog);
   }
}



