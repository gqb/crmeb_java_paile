package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.UserTask;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserTaskRequest;
import com.zbkj.common.request.course.UserTaskSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.UserTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/user-task")
@Api(tags = "用户-任务") //配合swagger使用

public class UserTaskController {

    @Autowired
    private UserTaskService userTaskService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-08-14
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<UserTask>>  getList(@Validated UserTaskSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<UserTask> userTaskCommonPage = CommonPage.restPage(userTaskService.getList(request, pageParamRequest));
        return CommonResult.success(userTaskCommonPage);
    }

    /**
     * 新增
     * @param userTaskRequest 新增参数
     * @author Mr.guqianbin
     * @since 2022-08-14
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@Validated UserTaskRequest userTaskRequest){
        UserTask userTask = new UserTask();
        BeanUtils.copyProperties(userTaskRequest, userTask);

        if(userTaskService.save(userTask)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-14
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(userTaskService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param id integer id
     * @param userTaskRequest 修改参数
     * @author Mr.guqianbin
     * @since 2022-08-14
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestParam Integer id, @Validated UserTaskRequest userTaskRequest){
        UserTask userTask = new UserTask();
        BeanUtils.copyProperties(userTaskRequest, userTask);
        userTask.setId(id);

        if(userTaskService.updateById(userTask)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-14
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<UserTask> info(@RequestParam(value = "id") Integer id){
        UserTask userTask = userTaskService.getById(id);
        return CommonResult.success(userTask);
   }
}



