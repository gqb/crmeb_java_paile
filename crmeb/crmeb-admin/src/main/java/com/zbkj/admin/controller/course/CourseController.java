package com.zbkj.admin.controller.course;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.ObjectUtil;
import com.zbkj.common.model.course.Course;
import com.zbkj.common.model.system.SystemAttachment;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseCodeRequest;
import com.zbkj.common.request.course.CourseRequest;
import com.zbkj.common.request.course.CourseSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseResponse;
import com.zbkj.service.service.SystemAttachmentService;
import com.zbkj.service.service.course.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;


/**
 * 前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/course")
@Api(tags = "课程") //配合swagger使用

public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private SystemAttachmentService attachmentService;




    /**
     * 分页显示
     *
     * @param request          搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseResponse>> getList(@Validated CourseSearchRequest request, @Validated PageParamRequest pageParamRequest) {
        CommonPage<CourseResponse> courseCommonPage = CommonPage.restPage(courseService.getList(request, pageParamRequest));
        return CommonResult.success(courseCommonPage);
    }

    /**
     * 新增
     *
     * @param courseRequest 新增参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@RequestBody @Validated CourseRequest courseRequest) {
        return courseService.addCourse(courseRequest);
    }

    /**
     * 删除
     *
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public CommonResult<String> delete(@RequestBody @PathVariable Integer id, @RequestParam(value = "type", required = false, defaultValue = "recycle")String type) {
        return courseService.deleteCourse(id,type);

    }

    /**
     * 修改
     *
     * @param courseRequest 修改参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<Map<String,Object>> update(@RequestBody CourseRequest courseRequest) {
       return courseService.updateCourse(courseRequest);
    }


    @ApiOperation(value = "上架下架")
    @RequestMapping(value = "/onlineOff", method = RequestMethod.POST)
    public CommonResult<Map<String,Object>> onlineOff(@RequestBody CourseRequest request) {
        return courseService.onlineOff(request.getId(),request.getIsShow());
    }
    /**
     * 查询信息
     *
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<CourseResponse> info(@RequestParam(value = "id") Integer id) {
        CourseResponse course = courseService.detail(id);
        return CommonResult.success(course);
    }
}



