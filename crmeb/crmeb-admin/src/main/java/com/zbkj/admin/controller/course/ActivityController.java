package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.Activity;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.ActivityRequest;
import com.zbkj.common.request.course.ActivitySearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.ActivityResponse;
import com.zbkj.service.service.course.ActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/activity")
@Api(tags = "活动") //配合swagger使用

public class ActivityController {

    @Autowired
    private ActivityService activityService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<ActivityResponse>> getList(@Validated ActivitySearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<ActivityResponse> activityCommonPage = CommonPage.restPage(activityService.getList(request, pageParamRequest));
        return CommonResult.success(activityCommonPage);
    }

    /**
     * 新增
     * @param activityRequest 新增参数
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<Map<String,Object>> save(@RequestBody @Validated ActivityRequest activityRequest){
        return CommonResult.success(activityService.add(activityRequest));
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(activityService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param activityRequest 修改参数
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<Map<String,Object>> update(@RequestBody @Validated ActivityRequest activityRequest){
        return CommonResult.success(activityService.update(activityRequest));

    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<ActivityResponse> info(@RequestParam(value = "id") Integer id){
        return CommonResult.success(activityService.detail(id));
   }
}



