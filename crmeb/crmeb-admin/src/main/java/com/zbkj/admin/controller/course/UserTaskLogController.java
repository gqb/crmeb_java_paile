package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.UserTaskLog;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserTaskLogRequest;
import com.zbkj.common.request.course.UserTaskLogSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.UserTaskLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/user-task-log")
@Api(tags = "用户-任务日志") //配合swagger使用

public class UserTaskLogController {

    @Autowired
    private UserTaskLogService userTaskLogService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-08-14
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<UserTaskLog>> getList(@Validated UserTaskLogSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<UserTaskLog> userTaskLogCommonPage = CommonPage.restPage(userTaskLogService.getList(request, pageParamRequest));
        return CommonResult.success(userTaskLogCommonPage);
    }
}



