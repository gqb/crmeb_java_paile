package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.CourseLabel;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseLabelRequest;
import com.zbkj.common.request.course.CourseLabelSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.CourseLabelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/course-label")
@Api(tags = "课程标签") //配合swagger使用

public class CourseLabelController {

    @Autowired
    private CourseLabelService courseLabelService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-08-10
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseLabel>>  getList(@Validated CourseLabelSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<CourseLabel> courseLabelCommonPage = CommonPage.restPage(courseLabelService.getList(request, pageParamRequest));
        return CommonResult.success(courseLabelCommonPage);
    }

    /**
     * 新增
     * @param courseLabelRequest 新增参数
     * @author Mr.guqianbin
     * @since 2022-08-10
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@Validated CourseLabelRequest courseLabelRequest){
        CourseLabel courseLabel = new CourseLabel();
        BeanUtils.copyProperties(courseLabelRequest, courseLabel);

        if(courseLabelService.save(courseLabel)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-10
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(courseLabelService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param id integer id
     * @param courseLabelRequest 修改参数
     * @author Mr.guqianbin
     * @since 2022-08-10
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestParam Integer id, @Validated CourseLabelRequest courseLabelRequest){
        CourseLabel courseLabel = new CourseLabel();
        BeanUtils.copyProperties(courseLabelRequest, courseLabel);
        courseLabel.setId(id);

        if(courseLabelService.updateById(courseLabel)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-10
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<CourseLabel> info(@RequestParam(value = "id") Integer id){
        CourseLabel courseLabel = courseLabelService.getById(id);
        return CommonResult.success(courseLabel);
   }
}



