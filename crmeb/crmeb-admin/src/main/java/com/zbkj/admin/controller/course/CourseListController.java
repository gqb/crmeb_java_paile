package com.zbkj.admin.controller.course;

import cn.hutool.core.util.ObjectUtil;
import com.zbkj.common.model.course.CourseList;
import com.zbkj.common.model.system.SystemAttachment;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseListRequest;
import com.zbkj.common.request.course.CourseListSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseListResponse;
import com.zbkj.service.service.SystemAttachmentService;
import com.zbkj.service.service.course.CourseListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/course-list")
@Api(tags = "课程-课程目录") //配合swagger使用
public class CourseListController {

    @Autowired
    private CourseListService courseListService;

    @Autowired
    private SystemAttachmentService attachmentService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseList>> getList(@Validated CourseListSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<CourseList> courseListCommonPage = CommonPage.restPage(courseListService.getList(request, pageParamRequest));
        return CommonResult.success(courseListCommonPage);
    }

    /**
     * 新增
     * @param courseListRequest 新增参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@RequestBody @Validated CourseListRequest courseListRequest){
        return courseListService.add(courseListRequest);
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(courseListService.delete(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param courseListRequest 修改参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestBody @Validated CourseListRequest courseListRequest){
      return courseListService.update(courseListRequest);
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "章节详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<CourseListResponse> info(@RequestParam(value = "id") Integer id){
        CourseListResponse courseList = courseListService.detail(id);
        return CommonResult.success(courseList);
   }
}



