package com.zbkj.admin.controller.integral;

import com.zbkj.common.model.integral.IntegralProduct;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.integral.IntegralProductRequest;
import com.zbkj.common.request.integral.IntegralProductSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.integral.IntegralProductDetailResponse;
import com.zbkj.service.service.integral.IntegralProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/integral/product")
@Api(tags = "积分商品") //配合swagger使用
public class IntegralProductController {

    @Autowired
    private IntegralProductService integralProductService;


    public static String unicodeToString(String str) {

        Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
        Matcher matcher = pattern.matcher(str);
        char ch;
        while (matcher.find()) {
            ch = (char) Integer.parseInt(matcher.group(2), 16);
            str = str.replace(matcher.group(1), ch + "");
        }
        return str;
    }



    /**
     * 分页显示
     *
     * @param request          搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2021-07-20
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<IntegralProduct>> getList(@Validated IntegralProductSearchRequest request, @Validated PageParamRequest pageParamRequest) {
        CommonPage<IntegralProduct> integralProductCommonPage = CommonPage.restPage(integralProductService.getList(request, pageParamRequest));
        return CommonResult.success(integralProductCommonPage);
    }

    /**
     * 新增
     *
     * @param integralProductRequest 新增参数
     * @author Mr.Zhang
     * @since 2021-07-20
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@RequestBody @Validated IntegralProductRequest integralProductRequest) {
        log.info(integralProductRequest.toString());
        if (integralProductService.addProduct(integralProductRequest)) {
            return CommonResult.success();
        } else {
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     *
     * @param id Integer
     * @author Mr.Zhang
     * @since 2021-07-20
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.GET,RequestMethod.DELETE})
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id) {
        if (integralProductService.removeById(id)) {
            return CommonResult.success();
        } else {
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     *
     * @param integralProductRequest 修改参数
     * @author Mr.Zhang
     * @since 2021-07-20
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestParam Integer id, @RequestBody  IntegralProductRequest integralProductRequest) {
        integralProductRequest.setId(id);
        if (integralProductService.update(integralProductRequest)) {
            return CommonResult.success();
        } else {
            return CommonResult.failed();
        }
    }
    @ApiOperation(value = "更新状态")
    @RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
    public CommonResult<String> updateStatus(@RequestParam Integer id,@RequestParam Boolean isShow) {

        if (integralProductService.updateShow(id,isShow)) {
            return CommonResult.success();
        } else {
            return CommonResult.failed();
        }
    }
    /**
     * 积分商品详情
     *
     * @param id Integer
     * @author Mr.Zhang
     * @since 2021-07-20
     */
    @ApiOperation(value = "积分商品详情")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "normal-正常，video-视频")
    public CommonResult<IntegralProductDetailResponse> info(@PathVariable(value = "id") Integer id
            , @RequestParam(value = "type", defaultValue = "normal") String type) {
        IntegralProductDetailResponse integralProduct = integralProductService.detail(id);
        return CommonResult.success(integralProduct);
    }
}



