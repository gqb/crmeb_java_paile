package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.PlskAd;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.PlskAdRequest;
import com.zbkj.common.request.course.PlskAdSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.SystemAttachmentService;
import com.zbkj.service.service.course.PlskAdService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/plsk-ad")
@Api(tags = "派乐时刻") //配合swagger使用

public class PlskAdController {

    @Autowired
    private PlskAdService plskAdService;

    @Autowired
    private SystemAttachmentService systemAttachmentService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-09-04
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<PlskAd>> getList(@Validated PlskAdSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<PlskAd> plskAdCommonPage = CommonPage.restPage(plskAdService.getList(request, pageParamRequest));
        return CommonResult.success(plskAdCommonPage);
    }

    /**
     * 新增
     * @param plskAdRequest 新增参数
     * @author Mr.guqianbin
     * @since 2022-09-04
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@RequestBody @Validated PlskAdRequest plskAdRequest){
        log.info(plskAdRequest.toString());
        PlskAd plskAd = new PlskAd();
        BeanUtils.copyProperties(plskAdRequest, plskAd);
        plskAd.setSliderImage(systemAttachmentService.clearPrefix(plskAd.getSliderImage()));
        plskAd.setCreateTime(new Date());
        if(plskAdService.save(plskAd)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-09-04
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(plskAdService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param id integer id
     * @param plskAdRequest 修改参数
     * @author Mr.guqianbin
     * @since 2022-09-04
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update( @RequestBody @Validated PlskAdRequest plskAdRequest){
        PlskAd plskAd = new PlskAd();
        BeanUtils.copyProperties(plskAdRequest, plskAd);
        plskAd.setSliderImage(systemAttachmentService.clearPrefix(plskAd.getSliderImage()));
        if(plskAdService.updateById(plskAd)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-09-04
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<PlskAd> info(@RequestParam(value = "id") Integer id){
        PlskAd plskAd = plskAdService.getById(id);
        return CommonResult.success(plskAd);
   }
}



