package com.zbkj.admin.controller.course;

import com.zbkj.common.model.course.CourseCode;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseCodeRequest;
import com.zbkj.common.request.course.CourseCodeSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseCodeResponse;
import com.zbkj.service.service.course.CourseCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/course-code")
@Api(tags = "课程-课程码") //配合swagger使用

public class CourseCodeController {

    @Autowired
    private CourseCodeService courseCodeService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseCodeResponse>> getList(@Validated CourseCodeSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<CourseCodeResponse> courseCodeCommonPage = CommonPage.restPage(courseCodeService.getList(request, pageParamRequest));
        return CommonResult.success(courseCodeCommonPage);
    }
    /**
     * 生成课程码
     *
     * @param courseCodeRequest
     * @author Mr.guqianbin
     * @since 2022-07-22
     */
    @ApiOperation(value = "生成课程码")
    @RequestMapping(value = "/generateCode", method = RequestMethod.POST)
    public CommonResult<String> generateCode(@Validated CourseCodeRequest courseCodeRequest) {
        return CommonResult.success(courseCodeService
                .generateCode(courseCodeRequest.getCourseId(),courseCodeRequest.getExchangeCount()));
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(courseCodeService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }
    /**
     * 删除
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "生成积分二维码")
    @ApiImplicitParams({
            @ApiImplicitParam(name="courseId",value = "课程id"),
            @ApiImplicitParam(name="count",value = "生成数量"),
            @ApiImplicitParam(name="integral",value = "二维码的积分值")
    })
    @RequestMapping(value = "/generateQRCode", method = RequestMethod.POST)
    public CommonResult<List<String>> generateQRCode(@RequestParam(value = "courseId") Integer courseId,
                                             @RequestParam(value = "count") int count
            ,@RequestParam(value = "integral")int integral) throws IOException {
        return CommonResult.success(courseCodeService.generateCourseIntegralQRCode(courseId,count,integral));
    }
}



