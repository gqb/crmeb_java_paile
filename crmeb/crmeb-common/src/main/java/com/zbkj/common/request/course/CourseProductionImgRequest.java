package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CourseProductionImg对象", description="")
public class CourseProductionImgRequest implements Serializable {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(value = "课程作品id")
    private Integer courseProductionId;


    @ApiModelProperty(value = "图片上传后的id")
    private Integer imgId;


}
