package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserBaby对象", description="")
public class UserBabyRequest implements Serializable {

    private static final long serialVersionUID=1L;
    @ApiModelProperty(value = "宝贝id")
    private Integer bid;

    @ApiModelProperty(value = "用户头像")
    private String avatar;
    @ApiModelProperty(value = "用户id")
    private Integer uid;
    @ApiModelProperty(value = "生日,yyyy-MM-dd")
    private String birthday;

    @ApiModelProperty(value = "性别，0未知，1男，2女，3保密")
    private Integer sex;

    @ApiModelProperty(value = "0未上学、1幼儿园、2小学、3初中、4高中")
    private Integer schoolLevel;

    @ApiModelProperty(value = "1年纪、2年级以此类推")
    private Integer level;
    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "是否是儿童")
    private Boolean isKid;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "区")
    private String area;

    @ApiModelProperty(value = "身份证")
    private String idCard;
}
