package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_user_level_unlock")
@ApiModel(value="UserLevelUnlock对象", description="")
public class UserLevelUnlockSearchRequest implements Serializable {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(value = "用户id")
    private Integer uid;

    private Integer userLevelId;


}
