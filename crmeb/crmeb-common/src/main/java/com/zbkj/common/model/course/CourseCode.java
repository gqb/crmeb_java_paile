package com.zbkj.common.model.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_course_code")
@ApiModel(value="CourseCode对象", description="")
public class CourseCode implements Serializable {

    private static final long serialVersionUID=1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer courseId;

    @ApiModelProperty(value = "课程码")
    private String code;

    @ApiModelProperty(value = "课程码状态（0：未兑换；1：已兑换；）")
    private Boolean status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "1：课程码，2课程积分二维码，3活动签到码")
    private Integer type;

    @ApiModelProperty(value = "积分二维码的积分值")
    private Integer codeValue;

    @ApiModelProperty(value = "关联id，type是3时为研学商品id")
    private Integer linkId;

    @ApiModelProperty(value = "二维码有效开始时间，type为3")
    private Date beginDate;

    @ApiModelProperty(value = "二维码有效结束时间，type为3")
    private Date endDate;

    @ApiModelProperty(value = "兑换数量")
    private Integer exchanged;
    @ApiModelProperty(value = "课程码可兑换次数")
    private Integer exchangeCount;

}
