package com.zbkj.common.model.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_course_description")
@ApiModel(value="CourseDescription对象", description="")
public class CourseDescription implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "详情富文本")
    private String description;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "1课程详情富文本2课程列表详情富文本")
    private Integer type;

    @ApiModelProperty(value = "关联的id")
    private Integer linkId;


}
