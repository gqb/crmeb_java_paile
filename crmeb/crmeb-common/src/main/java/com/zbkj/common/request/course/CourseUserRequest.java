package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CourseUser对象", description="")
public class CourseUserRequest implements Serializable {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(value = "课程id")
    @NotNull(message = "课程id不能为空")
    private Integer courseId;


    @ApiModelProperty(value = "课程码")
    @NotBlank(message = "课程码不能为空")
    private String courseCode;

}
