package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CourseCode对象", description="")
public class CourseCodeSearchRequest implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    private Integer courseId;

    @ApiModelProperty(value = "课程码")
    private Integer code;

    @ApiModelProperty(value = "课程码状态（0：未兑换；1：已兑换；）")
    private Boolean status;
    @ApiModelProperty(value = "用户id")
    private Integer uid;
    @ApiModelProperty(value = "1：课程码，2课程积分二维码")
    private Integer type;



}
