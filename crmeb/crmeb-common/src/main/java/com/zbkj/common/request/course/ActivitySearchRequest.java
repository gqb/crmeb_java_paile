package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_activity")
@ApiModel(value="Activity对象", description="")
public class ActivitySearchRequest implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "签到码")
    private String code;

    @ApiModelProperty(value = "1：研学活动商品")
    private Integer type;

    @ApiModelProperty(value = "关联id")
    private Integer linkId;

    @ApiModelProperty(value = "二维码有效开始时间，")
    private Date beginDate;

    @ApiModelProperty(value = "二维码有效结束时间")
    private Date endDate;

    @ApiModelProperty(value = "积分二维码的积分值")
    private Integer codeValue;

    @ApiModelProperty(value = "活动名称")
    private String name;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}
