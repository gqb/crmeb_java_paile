package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zbkj.common.model.course.CourseLabel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Course对象", description="")
public class CourseRequest implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课程名")
    private String name;

    @ApiModelProperty(value = "课程副标题")
    private String subName;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "是否删除")
    private Boolean isDel;

    @ApiModelProperty(value = "分类id")
    @NotNull(message = "分类id不能为空")
    private Integer cateId;

    @ApiModelProperty(value = "课程封面图")
    private String image;

    @ApiModelProperty(value = "课程封面id")
    private Integer imageAttId;
    @ApiModelProperty(value = "课程码数量")
    private Integer codeCount;

    @ApiModelProperty(value = "是否上架")
    private Boolean isShow;
    @ApiModelProperty(value = "是否回收站")
    private Boolean isRecycle;
    @ApiModelProperty(value = "课程章节数")
    private Integer listCount;
    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "课程详情富文本")
    private String description;

    @ApiModelProperty(value = "课程标签")
    private List<CourseLabelRequest> courseLabelList=new ArrayList<>();



}
