package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CourseProduction对象", description="")
public class CourseProductionSearchRequest implements Serializable {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(value = "课程id")
    private Integer courseId;
    private Integer id;

    @ApiModelProperty(value = "某个用户id")
    private Integer uid;

    @ApiModelProperty(value = "1:时间排序；2点赞排序")
    private Integer order=1;


}
