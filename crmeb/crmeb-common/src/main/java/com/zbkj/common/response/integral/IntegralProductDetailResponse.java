package com.zbkj.common.response.integral;

import com.zbkj.common.model.integral.IntegralProduct;
import com.zbkj.common.model.product.StoreProductAttr;
import com.zbkj.common.model.product.StoreProductAttrValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="IntegralProductDetailResponse对象", description="积分商品详情响应对象")
public class IntegralProductDetailResponse {

    @ApiModelProperty(value = "商品属性")
    private List<StoreProductAttr> attr;
    @ApiModelProperty(value = "商品属性详情数组")
    private List<StoreProductAttrValue> attrValue;

    @ApiModelProperty(value = "管理端用于映射attrResults")
    private List<HashMap<String,Object>> attrValues;

    @ApiModelProperty(value = "商品信息")
    private IntegralProduct integralProduct;

    @ApiModelProperty(value = "收藏标识")
    private Boolean userCollect;
}
