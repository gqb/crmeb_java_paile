package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_activity")
@ApiModel(value="Activity对象", description="")
public class ActivityRequest implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    @ApiModelProperty(value = "签到码")
    private String code;

    @ApiModelProperty(value = "1：研学活动商品")
    private Integer type;

    @ApiModelProperty(value = "关联id")
    private Integer linkId;


    @ApiModelProperty(value = "积分二维码的积分值")
    @NotNull(message = "签到积分值不能为空")
    private Integer codeValue;

    @ApiModelProperty(value = "活动名称")
    @NotBlank(message = "活动标题不能为空")
    private String name;

    @ApiModelProperty(value = "商品封面图片")
    private String image;

    @ApiModelProperty(value = "距离，单位米")
    private Integer distance;
    @ApiModelProperty(value = "关联商品id")
    private Integer productId;

}
