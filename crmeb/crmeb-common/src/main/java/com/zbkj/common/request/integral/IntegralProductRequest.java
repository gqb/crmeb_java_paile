package com.zbkj.common.request.integral;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.product.StoreProductAttr;
import com.zbkj.common.request.StoreProductAttrValueRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author Mr.Zhang
 * @since 2021-07-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_integral_product")
@ApiModel(value = "IntegralProduct对象", description = "")
public class IntegralProductRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "商品id", required = true)
    private Integer productId;
    @ApiModelProperty(value = "推荐图")
    private String image;

    @ApiModelProperty(value = "轮播图",name = "imagess")
    private String slider_image;
    @ApiModelProperty(value = "商品名称")
    private String productName;
    @ApiModelProperty(value = "活动标题")
    private String title;

    @ApiModelProperty(value = "简介")
    private String info;
    @ApiModelProperty(value = "购买方式:0积分购买1积分+金额购买", required = true)
    private Integer type;
    @ApiModelProperty(value = "分类id|逗号分隔", required = true)
    @NotBlank(message = "商品分类不能为空")
    private String cateId;
    @ApiModelProperty(value = "积分价格")
    private Integer integralPrice;
    @ApiModelProperty(value = "商品人民币的价格")
    private BigDecimal price;
    @ApiModelProperty(value = "成本")
    private BigDecimal cost;

    @ApiModelProperty(value = "原价")
    private BigDecimal otPrice;
    @ApiModelProperty(value = "排序", required = true)
    private Integer sort;

    @ApiModelProperty(value = "库存")
    private Integer stock;

    @ApiModelProperty(value = "销量")
    private Integer sales;

    @ApiModelProperty(value = "重量")
    private BigDecimal weight;

    @ApiModelProperty(value = "体积")
    private BigDecimal volume;

    @ApiModelProperty(value = "显示")
    private Boolean isShow;
    @ApiModelProperty(value = "单位名")
    private String unitName;

    @ApiModelProperty(value = "邮费")
    private BigDecimal postage;

    @ApiModelProperty(value = "内容")
    private String description;
    @ApiModelProperty(value = "运费模板ID")
    private Integer tempId;
    @ApiModelProperty(value = "是否包邮")
    private Boolean isPostage;
    @ApiModelProperty(value = "规格 0单 1多")
//    @NotNull(message = "商品规格不能为空")
    private Boolean specType;

    @ApiModelProperty(value = "商品属性")
    private List<StoreProductAttr> attr;

    @ApiModelProperty(value = "商品属性详情")
    private List<StoreProductAttrValueRequest> attrValue;

    @ApiModelProperty(value = "商品描述")
    private String content;

    @Override
    public String toString() {
        return "IntegralProductRequest{" +
                "id=" + id +
                ", productId=" + productId +
                ", image='" + image + '\'' +
                ", slider_image='" + slider_image + '\'' +
                ", productName='" + productName + '\'' +
                ", title='" + title + '\'' +
                ", info='" + info + '\'' +
                ", type=" + type +
                ", integralPrice=" + integralPrice +
                ", price=" + price +
                ", cost=" + cost +
                ", otPrice=" + otPrice +
                ", sort=" + sort +
                ", stock=" + stock +
                ", sales=" + sales +
                ", weight=" + weight +
                ", volume=" + volume +
                ", isShow=" + isShow +
                ", unitName='" + unitName + '\'' +
                ", postage=" + postage +
                ", description='" + description + '\'' +
                ", tempId=" + tempId +
                ", isPostage=" + isPostage +
                ", specType=" + specType +
                ", attr=" + attr +
                ", attrValue=" + attrValue +
                ", content='" + content + '\'' +
                '}';
    }
}
