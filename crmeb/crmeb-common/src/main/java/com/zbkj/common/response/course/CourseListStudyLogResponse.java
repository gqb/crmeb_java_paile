package com.zbkj.common.response.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_course_list_study_log")
@ApiModel(value="CourseListStudyLog对象", description="")
public class CourseListStudyLogResponse implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "章节状态（0：未学习；1：学习中；2：已完成）")
    private Integer status;

    @ApiModelProperty(value = "课程id")
    private Integer courseId;
    @ApiModelProperty(value = "课程目录id")
    private Integer courseListId;
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "课程名")
    private String name;

    @ApiModelProperty(value = "视频长度、秒")
    private Integer videoLength;

    @ApiModelProperty(value = "章节副标题")
    private String subName;

    @ApiModelProperty(value = "已学习时长")
    private Integer studyLength;
    @ApiModelProperty(value = "视频地址")
    private String video;
    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "播放量")
    private Integer playCount;
    @ApiModelProperty(value = "课程材料")
    private String material;
    @ApiModelProperty(value = "课程目标")
    private String target;

}
