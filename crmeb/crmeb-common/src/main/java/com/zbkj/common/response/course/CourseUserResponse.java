package com.zbkj.common.response.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.course.CourseLabel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_course_user")
@ApiModel(value="CourseUser对象", description="")
public class CourseUserResponse implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课程名")
    private String name;

    @ApiModelProperty(value = "课程副标题")
    private String subName;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "分类id")
    private Integer cateId;

    @ApiModelProperty(value = "课程id")
    private Integer courseId;

    @ApiModelProperty(value = "课程封面图")
    private String image;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "课程码")
    private String courseCode;

    @ApiModelProperty(value = "播放量")
    private Integer playCount;

    @ApiModelProperty(value = "课程章节数")
    private Integer listCount;
    @ApiModelProperty(value = "课程标签数组")
    private List<CourseLabel> labels=new ArrayList<>();

    @ApiModelProperty(value = "课程兑换后的，我的课程目录")
    private List<CourseListStudyLogResponse> courseListStudyLogs=new ArrayList<>();

}
