package com.zbkj.common.request.course;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-09-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="PlskAd对象", description="")
public class PlskAdSearchRequest implements Serializable {

    private static final long serialVersionUID=1L;




    @ApiModelProperty(value = "1新品时刻、2活动时刻")
    private Integer type;

}
