package com.zbkj.common.model.product;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 商品属性值表
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_store_product_attr_value")
@ApiModel(value="StoreProductAttrValue对象", description="商品属性值表")
public class StoreProductAttrValue implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "attrId")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @ApiModelProperty(value = "商品属性索引值 (attr_value|attr_value[|....])")
    private String suk;

    @ApiModelProperty(value = "属性对应的库存")
    private Integer stock;

    @ApiModelProperty(value = "销量")
    private Integer sales;

    @ApiModelProperty(value = "属性金额")
    private BigDecimal price;

    @ApiModelProperty(value = "图片")
    private String image;

    @TableField(value = "`unique`")
    @ApiModelProperty(value = "唯一值")
    private String unique;

    @ApiModelProperty(value = "成本价")
    private BigDecimal cost;

    @ApiModelProperty(value = "商品条码")
    private String barCode;

    @ApiModelProperty(value = "原价")
    private BigDecimal otPrice;

    @ApiModelProperty(value = "重量")
    private BigDecimal weight;

    @ApiModelProperty(value = "体积")
    private BigDecimal volume;

    @ApiModelProperty(value = "一级返佣")
    private BigDecimal brokerage;

    @ApiModelProperty(value = "二级返佣")
    private BigDecimal brokerageTwo;

    @ApiModelProperty(value = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;

    @ApiModelProperty(value = "活动限购数量")
    private Integer quota;

    @ApiModelProperty(value = "活动限购数量显示")
    private Integer quotaShow;

    @ApiModelProperty(value = "产品属性值和属性名对应关系")
    private String attrValue;

    @ApiModelProperty(value = "是否删除,0-否，1-是")
    private Boolean isDel;

    @ApiModelProperty(value = "积分商品的积分价格")
    private Integer integralPrice;
    @ApiModelProperty(value = "企业优惠价格，企业用户的优惠价格")
    private BigDecimal enterprisePrice;
    @ApiModelProperty(value = "不要积分的商品价格")
    private BigDecimal noIntegralPrice;
    @ApiModelProperty(value = "购买商品获得积分")
    private Integer giveIntegral;

    @ApiModelProperty(value = "儿童价格")
    private BigDecimal kidPrice;
    @ApiModelProperty(value = "成人价格")
    private BigDecimal manPrice;

    private Integer kidCount;

    private Integer manCount;

    @ApiModelProperty(value = "纬度")
    private String longitude;

    @ApiModelProperty(value = "经度")
    private String latitude;

    @ApiModelProperty(value = "研学活动地址")
    private String address;


    @ApiModelProperty(value = "签到开始时间")
    private Date signStartTime;
    @ApiModelProperty(value = "签到结束时间")
    private Date signEndTime;

    @ApiModelProperty(value = "该规格售卖截止时间")
    private Date deadline;
}
