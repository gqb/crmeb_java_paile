package com.zbkj.common.response.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zbkj.common.model.course.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Course响应对象", description="")
public class CourseResponse implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课程名")
    private String name;

    @ApiModelProperty(value = "课程副标题")
    private String subName;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "是否删除")
    private Boolean isDel;

    @ApiModelProperty(value = "分类id")
    private String cateId;

    @ApiModelProperty(value = "课程封面图")
    private String image;

    @ApiModelProperty(value = "课程码数量")
    private Integer codeCount;
    @ApiModelProperty(value = "富文本详情")
    private String description;


    @ApiModelProperty(value = "播放量")
    private Integer playCount;

    @ApiModelProperty(value = "是否兑换该课程")
    private Boolean isExchange=false;

    @ApiModelProperty(value = "兑换的课程信息")
    private CourseUser courseUser;
    @ApiModelProperty(value = "积分二维码数量")
    private Integer qrCodeCount;
    @ApiModelProperty(value = "是否上架")
    private Boolean isShow;
    @ApiModelProperty(value = "是否回收站")
    private Boolean isRecycle;
    @ApiModelProperty(value = "课程章节数")
    private Integer listCount;
    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "课程目录，未兑换前的或者管理端使用的")
    private List<CourseListResponse> courseLists=new ArrayList<>();

    @ApiModelProperty(value = "课程码，后台管理端的")
    private List<CourseCode> courseCodeList=new ArrayList<>();
    @ApiModelProperty(value = "课程兑换后的，我的课程目录")
    private List<CourseListStudyLogResponse> courseListStudyLogs=new ArrayList<>();

    @ApiModelProperty(value = "课程标签数组")
    private List<CourseLabel> labels=new ArrayList<>();
}
