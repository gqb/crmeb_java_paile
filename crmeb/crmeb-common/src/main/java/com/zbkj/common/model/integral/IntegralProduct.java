package com.zbkj.common.model.integral;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2021-07-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_integral_product")
@ApiModel(value="IntegralProduct对象", description="")
public class IntegralProduct implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "商品id")
    private Integer productId;

    @ApiModelProperty(value = "推荐图")
    private String image;

    @ApiModelProperty(value = "轮播图")
    private String sliderImage;

    @ApiModelProperty(value = "商品名名称")
    private String productName;

    @ApiModelProperty(value = "活动标题")
    private String title;
    @ApiModelProperty(value = "分类id|逗号分隔")
    private String cateId;
    @ApiModelProperty(value = "简介")
    private String info;

    @ApiModelProperty(value = "购买方式:0积分购买1积分+金额购买")
    private Integer type;

    @ApiModelProperty(value = "积分价格")
    private Integer integralPrice;

    private BigDecimal price;
    @ApiModelProperty(value = "成本")
    private BigDecimal cost;

    @ApiModelProperty(value = "原价")
    private BigDecimal otPrice;
    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "库存")
    private Integer stock;

    @ApiModelProperty(value = "销量")
    private Integer sales;
    @ApiModelProperty(value = "重量")
    private BigDecimal weight;

    @ApiModelProperty(value = "体积")
    private BigDecimal volume;
    @ApiModelProperty(value = "添加时间")
    private Date createTime;

    @ApiModelProperty(value = "显示")
    private Boolean isShow;
    @ApiModelProperty(value = "单位名")
    private String unitName;

    @ApiModelProperty(value = "邮费")
    private BigDecimal postage;

    @ApiModelProperty(value = "内容")
    private String description;
    @ApiModelProperty(value = "运费模板ID")
    private Integer tempId;
    @ApiModelProperty(value = "是否包邮")
    private Boolean isPostage;
    @ApiModelProperty(value = "商品详情")
    @TableField(exist = false)
    private String content;
    @ApiModelProperty(value = "规格 0单 1多")
    private Boolean specType;

    @TableField(exist = false)
    @ApiModelProperty(value = "商品最低商品价格")
    private String minPrice;

    @TableField(exist = false)
    @ApiModelProperty(value = "商品最低积分价格")
    private String minIntegralPrice;

}
