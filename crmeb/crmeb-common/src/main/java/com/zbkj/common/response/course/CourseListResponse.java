package com.zbkj.common.response.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_course_list")
@ApiModel(value="CourseList对象", description="")
public class CourseListResponse implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课程id")
    private Integer courseId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "课程名")
    private String name;

    @ApiModelProperty(value = "视频长度、秒")
    private Integer videoLength;

    @ApiModelProperty(value = "章节副标题")
    private String subName;
    @ApiModelProperty(value = "课程封面图")
    private String image;

    @ApiModelProperty(value = "视频地址")
    private String video;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "富文本详情")
    private String description;


    //以下是已订阅课程后，这三个字段是有值的
    @ApiModelProperty(value = "已订阅课程后会有： 用户id")
    private Integer uid;

    @ApiModelProperty(value = "已订阅课程后会有：已学习时长")
    private Integer studyLength;

    @ApiModelProperty(value = "已订阅课程后会有：章节状态（0：未学习；1：学习中；2：已完成）")
    private Integer status;
    @ApiModelProperty(value = "课程材料")
    private String material;
    @ApiModelProperty(value = "课程目标")
    private String target;
    @ApiModelProperty(value = "播放量")
    private Integer playCount;
    @ApiModelProperty
    private Integer courseListStudyLogId;
}
