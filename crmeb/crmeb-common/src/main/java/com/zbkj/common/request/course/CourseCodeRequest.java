package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CourseCode对象", description = "")
public class CourseCodeRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程id:不能为空")
    @NotNull(message = "课程id不能为空")
    private Integer courseId;

    @ApiModelProperty(value = "1：课程码，2课程积分二维码")
    private Integer type;

    @ApiModelProperty(value = "课程码可兑换次数")
    private Integer exchangeCount;


}
