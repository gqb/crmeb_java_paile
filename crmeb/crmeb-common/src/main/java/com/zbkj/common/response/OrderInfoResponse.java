package com.zbkj.common.response;

import com.zbkj.common.model.course.StoreOrderMember;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 订单详情响应对象
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="OrderInfoResponse对象", description="订单详情响应对象")
public class OrderInfoResponse implements Serializable {

    private static final long serialVersionUID=1L;

//    @ApiModelProperty(value = "订单id")
//    private Integer orderId;
    @ApiModelProperty(value = "attrId")
    private Integer attrId;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

//    @ApiModelProperty(value = "购买东西的详细信息")
//    private StoreCartResponse info;

    @ApiModelProperty(value = "商品数量")
    private Integer cartNum;

//    @ApiModelProperty(value = "唯一id")
//    @TableField(value = "`unique`")
//    private String unique;

    @ApiModelProperty(value = "商品图片")
    private String image;

    @ApiModelProperty(value = "商品名称")
    private String storeName;

    @ApiModelProperty(value = "商品价格")
    private BigDecimal price;
    @ApiModelProperty(value = "积分商品积分价格")
    private Integer integralPrice;
    /** 积分商品id */
    private Integer integralProductId;


    @ApiModelProperty(value = "是否评价")
    private Integer isReply;

    @ApiModelProperty(value = "规格属性值")
    private String sku;

    @ApiModelProperty(value = "是否是研学报名商品")
    private Boolean isResearch;
    @ApiModelProperty(value = "儿童价格")
    private BigDecimal kidPrice;
    @ApiModelProperty(value = "成人价格")
    private BigDecimal manPrice;
    private Integer kidCount=0;

    private Integer manCount=0;
    @ApiModelProperty(value = "参加研学活动成员")
    private List<StoreOrderMember> orderMembers=new ArrayList<>();
}
