package com.zbkj.common.model.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_user_task")
@ApiModel(value="UserTask对象", description="")
public class UserTask implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "1一次性任务、2每日、3每周、4每月、5每季度、6年")
    private Integer type;

    @ApiModelProperty(value = "任务名称")
    private String name;

    @ApiModelProperty(value = "完成后获得积分")
    private Integer integral;

    @ApiModelProperty(value = "是否显示，0未显示，1显示")
    private Boolean isShow;

    @ApiModelProperty(value = "是否删除,0=未删除,1=删除")
    private Boolean isDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "上限次数")
    private Integer limitCount;


}
