package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CourseList对象", description="")
public class CourseListRequest implements Serializable {

    private static final long serialVersionUID=1L;


    private Integer id;

    @ApiModelProperty(value = "课程id")
    private Integer courseId;


    @ApiModelProperty(value = "章节名名")
    private String name;

    @ApiModelProperty(value = "章节副标题")
    private String subName;

    @ApiModelProperty(value = "视频的文件路径，oss复制过来")
    private String video;
    @ApiModelProperty(value = "视频长度，需要手动输入")
    private Integer videoLength=1;
    @ApiModelProperty(value = "课程封面图地址")
    private String image;
    @ApiModelProperty(value = "课程封面id")
    private Integer imageAttId;

    @ApiModelProperty(value = "章节详情富文本")
    private String description;
    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "课程材料")
    private String material;
    @ApiModelProperty(value = "课程目标")
    private String target;

}
