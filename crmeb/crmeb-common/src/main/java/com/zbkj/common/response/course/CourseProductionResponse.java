package com.zbkj.common.response.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CourseProduction对象", description="")
public class CourseProductionResponse implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课程id")
    private Integer courseId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "用户id")
    private Integer uid;
    @ApiModelProperty(value = "用户头像")
    private String avatar;
    private String nickname;
    @ApiModelProperty(value = "点赞")
    private Integer likeCount;
    @ApiModelProperty(value = "录音文件路径")
    private String audioFile;

    @ApiModelProperty(value = "作品描述")
    private String description;

    @ApiModelProperty(value = "作品的标题")
    private String title;

    @ApiModelProperty(value = "是否点赞")
    private Boolean liked=false;

    private List<String> imgPaths=new ArrayList<>();

}
