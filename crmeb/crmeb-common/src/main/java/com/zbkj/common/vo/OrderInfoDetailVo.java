package com.zbkj.common.vo;

import com.zbkj.common.model.course.StoreOrderMember;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单详情Vo对象
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Data
public class OrderInfoDetailVo {

    /** 商品id */
    private Integer productId;

    /** 商品名称 */
    private String productName;

    /** 规格属性id */
    private Integer attrValueId;

    /** 商品图片 */
    private String image;

    /** sku */
    private String sku;

    /** 单价 */
    private BigDecimal price;

    /** 购买数量 */
    private Integer payNum;

    /** 重量 */
    private BigDecimal weight;

    /** 体积 */
    private BigDecimal volume;

    /** 运费模板ID */
    private Integer tempId;

    /** 获得积分 */
    private Integer giveIntegral;

    /** 是否评价 */
    private Integer isReply;

    /** 是否单独分佣 */
    private Boolean isSub;

    /** 会员价 */
    private BigDecimal vipPrice;

    /** 商品类型:0-普通，1-秒杀，2-砍价，3-拼团，4-积分商品 */
    @ApiModelProperty(value = "商品类型:0-普通，1-秒杀，2-砍价，3-拼团，4-积分商品")
    private Integer productType;

    @ApiModelProperty(value = "企业优惠价格，企业用户的优惠价格")
    private BigDecimal enterprisePrice;
    @ApiModelProperty(value = "不要积分的商品价格")
    private BigDecimal noIntegralPrice;
    /**积分商品积分价格*/
    private Integer integralPrice=0;

    /** 积分商品id */
    private Integer integralProductId=0;

    @ApiModelProperty(value = "是否是研学报名商品")
    private Boolean isResearch=false;
    @ApiModelProperty(value = "儿童价格")
    private BigDecimal kidPrice;
    @ApiModelProperty(value = "成人价格")
    private BigDecimal manPrice;

    private Integer kidCount=0;

    private Integer manCount=0;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "活动地点地址")
    private String address;

    @ApiModelProperty(value = "活动签到开始时间")
    private Date signStartTime;
    @ApiModelProperty(value = "活动签到结束时间")
    private Date signEndTime;
    @ApiModelProperty(value = "该规格售卖截止时间")
    private Date deadline;
    @ApiModelProperty(value = "参加研学活动成员")
    private List<StoreOrderMember> orderMembers=new ArrayList<>();
}
