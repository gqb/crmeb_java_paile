package com.zbkj.common.request.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-09-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="PlskAd对象", description="")
public class PlskAdRequest implements Serializable {

    private static final long serialVersionUID=1L;


    private Integer id;
    @ApiModelProperty(value = "标题")
    private String name;

    @ApiModelProperty(value = "副标题")
    private String subName;

    @ApiModelProperty(value = "轮播图")
    private String sliderImage;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "标签")
    private String label;
    @ApiModelProperty(value = "内页描述")
    private String description;
    @ApiModelProperty(value = "1新品时刻、2活动时刻")
    private Integer type;
}
