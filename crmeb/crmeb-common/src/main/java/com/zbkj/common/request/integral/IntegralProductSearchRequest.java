package com.zbkj.common.request.integral;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author Mr.Zhang
 * @since 2021-07-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_integral_product")
@ApiModel(value = "IntegralProduct对象", description = "")
public class IntegralProductSearchRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "商品id")
    private Integer productId;
    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "购买方式:0积分购买1积分+金额购买")
    private Integer type = 0;
    @ApiModelProperty(value = "分类id|逗号分隔")
    private Integer cateId;
    @ApiModelProperty(value = "积分价格")
    private Integer integralPrice = 0;

    @ApiModelProperty(value = "商品人民币的价格")
    private BigDecimal price = new BigDecimal(0);

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "库存")
    private Integer stock;

    @ApiModelProperty(value = "销量")
    private Integer sales;


    @ApiModelProperty(value = "显示")
    private Boolean isShow;

    @ApiModelProperty(value = "isShow字段是否参与搜索，true：isShow的参与搜索")
    private Boolean isShowActive = false;
    @ApiModelProperty(value = "运费模板ID")
    private Integer tempId;


}
