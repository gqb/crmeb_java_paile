package com.zbkj.front.controller.course;

import com.zbkj.common.model.course.CourseUser;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseUserRequest;
import com.zbkj.common.request.course.CourseUserSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseResponse;
import com.zbkj.common.response.course.CourseUserResponse;
import com.zbkj.service.service.course.CourseListStudyLogService;
import com.zbkj.service.service.course.CourseUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/course-user")
@Api(tags = "课程-我的课程") //配合swagger使用

public class CourseUserController {

    @Autowired
    private CourseUserService courseUserService;

    @Autowired
    private CourseListStudyLogService courseListStudyLogService;
    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-26
     */
    @ApiOperation(value = "我的课程列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseUserResponse>> getList(@Validated CourseUserSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<CourseUserResponse> courseUserCommonPage = CommonPage.restPage(courseUserService.getList(request, pageParamRequest));
        return CommonResult.success(courseUserCommonPage);
    }

    /**
     * 兑换课程
     * @param courseUserRequest 兑换课程参数
     * @author Mr.Zhang
     * @since 2022-07-26
     */
    @ApiOperation(value = "兑换课程")
    @RequestMapping(value = "/exchangeCourse", method = RequestMethod.POST)
    public CommonResult<String> exchangeCourse(@RequestBody @Validated CourseUserRequest courseUserRequest){
        return courseUserService.exchangeCourse(courseUserRequest.getCourseCode(),courseUserRequest.getCourseId());
    }


//    /**
//     * 查询信息
//     * @param id Integer
//     * @author Mr.Zhang
//     * @since 2022-07-26
//     */
//    @ApiOperation(value = "详情")
//    @RequestMapping(value = "/info", method = RequestMethod.GET)
//    public CommonResult<CourseUser> info(@RequestParam(value = "id") Integer id){
//        CourseUser courseUser = courseUserService.getById(id);
//        return CommonResult.success(courseUser);
//   }

    /**
     * 课程学习进度更新 接口，用来更新学习到第几分钟
     * @author Mr.Zhang
     * @since 2022-07-26
     */
    @ApiOperation(value = "课程学习进度更新")
    @RequestMapping(value = "/updateProgress", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="courseListStudyLogId", value="courseListStudyLog的id", example = "1"),
            @ApiImplicitParam(name="studyLength", value="学习时间长度，秒", example = "1")
    })
    public CommonResult<String> updateProgress(@RequestParam(value = "courseListStudyLogId") Integer courseListStudyLogId
            ,@RequestParam(value = "studyLength")  Integer studyLength){
        return courseListStudyLogService.updateProgress(courseListStudyLogId,studyLength);
    }
    /**
     * 查询信息
     *
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "我的课程详情")
    @RequestMapping(value = "/courseUserDetail", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="id", value="CourseUser的id", example = "1")
    })
    public CommonResult<CourseResponse> myCourseDetail(@RequestParam(value = "id") Integer id) {
        CourseResponse course = courseUserService.detailFront(id);
        return CommonResult.success(course);
    }
}



