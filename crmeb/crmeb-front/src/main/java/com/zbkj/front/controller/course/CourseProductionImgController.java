package com.zbkj.front.controller.course;

import com.zbkj.common.model.course.CourseProductionImg;
import com.zbkj.common.model.system.SystemAttachment;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionImgRequest;
import com.zbkj.common.request.course.CourseProductionImgSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.SystemAttachmentService;
import com.zbkj.service.service.UserService;
import com.zbkj.service.service.course.CourseProductionImgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/course-production-img")
@Api(tags = "") //配合swagger使用

public class CourseProductionImgController {

    @Autowired
    private CourseProductionImgService courseProductionImgService;

    @Autowired
    private SystemAttachmentService attachmentService;
    @Autowired
    private UserService userService;
    /**
     * 新增
     * @param courseProductionImgRequest 新增参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "新增作品的图片")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@Validated CourseProductionImgRequest courseProductionImgRequest){
        CourseProductionImg courseProductionImg = new CourseProductionImg();
        BeanUtils.copyProperties(courseProductionImgRequest, courseProductionImg);
        courseProductionImg.setUid(userService.getUserIdException());
        SystemAttachment attachment=attachmentService.getById(courseProductionImgRequest.getImgId());
        courseProductionImg.setImage(attachment.getSattDir());
        if(courseProductionImgService.save(courseProductionImg)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "删除作品图片")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ApiImplicitParam(name = "id",value = "CourseProductionImg的id")
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(courseProductionImgService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

}



