package com.zbkj.front.controller.integral;

import com.zbkj.common.model.integral.IntegralProduct;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.integral.IntegralProductSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.integral.IntegralProductDetailH5Response;
import com.zbkj.front.service.ProductService;
import com.zbkj.service.service.integral.IntegralProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController(value = "frontIntegralProductController")
@RequestMapping("api/front/integral/product")
@Api(tags = "积分商品")
public class IntegralProductController {

    @Autowired
    private IntegralProductService integralProductService;
    @Autowired
    private ProductService productService;

    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<IntegralProduct>> getList(@Validated IntegralProductSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<IntegralProduct> integralProductCommonPage = CommonPage.restPage(integralProductService.getList(request, pageParamRequest));
        return CommonResult.success(integralProductCommonPage);
    }


    /**
     * 商品详情
     */
    @ApiOperation(value = "积分商品详情")
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "normal-正常，video-视频")
    public CommonResult<IntegralProductDetailH5Response> getDetail(@PathVariable(value = "id") Integer id,
                                                                   @RequestParam(value = "type", defaultValue = "normal") String type){
        return CommonResult.success(integralProductService.detailH5(id));
    }
}
