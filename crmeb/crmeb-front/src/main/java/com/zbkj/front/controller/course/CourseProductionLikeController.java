package com.zbkj.front.controller.course;

import com.zbkj.common.model.course.CourseProductionLike;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionLikeRequest;
import com.zbkj.common.request.course.CourseProductionLikeSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.CourseProductionLikeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/course-production-like")
@Api(tags = "课程-作品点赞") //配合swagger使用

public class CourseProductionLikeController {

    @Autowired
    private CourseProductionLikeService courseProductionLikeService;

//    /**
//     * 分页显示
//     * @param request 搜索条件
//     * @param pageParamRequest 分页参数
//     * @author Mr.guqianbin
//     * @since 2022-08-02
//     */
//    @ApiOperation(value = "分页列表") //配合swagger使用
//    @RequestMapping(value = "/list", method = RequestMethod.GET)
//    public CommonResult<CommonPage<CourseProductionLike>> getList(@Validated CourseProductionLikeSearchRequest request, @Validated PageParamRequest pageParamRequest){
//        CommonPage<CourseProductionLike> courseProductionLikeCommonPage = CommonPage.restPage(courseProductionLikeService.getList(request, pageParamRequest));
//        return CommonResult.success(courseProductionLikeCommonPage);
//    }

    /**
     * 新增
     * @author Mr.guqianbin
     * @since 2022-08-02
     */
    @ApiOperation(value = "点赞，重复点击表示点赞或者取消点赞")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> like(@RequestBody @Validated CourseProductionLikeRequest request){

        return courseProductionLikeService.like(request);

    }

}



