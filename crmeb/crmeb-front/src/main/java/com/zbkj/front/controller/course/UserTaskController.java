package com.zbkj.front.controller.course;

import com.zbkj.common.model.course.UserTask;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserTaskRequest;
import com.zbkj.common.request.course.UserTaskSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.UserTaskResponse;
import com.zbkj.service.service.course.UserTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/user-task")
@Api(tags = "我-我的任务") //配合swagger使用

public class UserTaskController {

    @Autowired
    private UserTaskService userTaskService;

    /**
     * 分页显示
     * @author Mr.guqianbin
     * @since 2022-08-14
     */
    @ApiOperation(value = "我的任务") //配合swagger使用
    @RequestMapping(value = "/todayTask", method = RequestMethod.GET)
    public CommonResult<List<UserTaskResponse>>  todayTask(){
        List<UserTaskResponse> userTaskResponses =userTaskService.getTodayTask();
        return CommonResult.success(userTaskResponses);
    }

    /**
     * 新增
     * @param taskId 新增参数
     * @author Mr.guqianbin
     * @since 2022-08-14
     */
    @ApiOperation(value = "完成任务")
    @RequestMapping(value = "/completeTask", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="taskId", value="完成任务的任务id", required = true),
    })
    public CommonResult<String> completeTask(@RequestParam Integer taskId){
        UserTask userTask = new UserTask();

        if(userTaskService.save(userTask)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }



}



