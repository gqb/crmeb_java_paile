package com.zbkj.front.controller.course;

import cn.hutool.core.util.ObjectUtil;
import com.zbkj.common.model.course.Course;
import com.zbkj.common.model.system.SystemAttachment;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseCodeRequest;
import com.zbkj.common.request.course.CourseRequest;
import com.zbkj.common.request.course.CourseSearchRequest;
import com.zbkj.common.request.course.QRCodeSweepRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseListResponse;
import com.zbkj.common.response.course.CourseResponse;
import com.zbkj.service.service.SystemAttachmentService;
import com.zbkj.service.service.course.CourseCodeService;
import com.zbkj.service.service.course.CourseListService;
import com.zbkj.service.service.course.CourseService;
import io.jsonwebtoken.impl.crypto.MacProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;


/**
 * 前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/course")
@Api(tags = "课程") //配合swagger使用

public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private SystemAttachmentService attachmentService;

    @Autowired
    private CourseListService courseListService;

    @Autowired
    private CourseCodeService courseCodeService;
    /**
     * 分页显示
     *
     * @param request          搜索条件
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "扫码") //配合swagger使用
    @RequestMapping(value = "/sweepQRCode", method = RequestMethod.POST)
    public CommonResult<Map<String,Object>> sweepQRCode(@RequestBody  @Validated QRCodeSweepRequest request) {
        return CommonResult.success(courseCodeService.sweepQRCode(request));
    }


    /**
     * 分页显示
     *
     * @param request          搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseResponse>> getList(@Validated CourseSearchRequest request, @Validated PageParamRequest pageParamRequest) {
        CommonPage<CourseResponse> courseCommonPage = CommonPage.restPage(courseService.getListFront(request, pageParamRequest));
        return CommonResult.success(courseCommonPage);
    }

    /**
     * 查询信息
     *
     * @param courseId Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "课程详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<CourseResponse> info(@RequestParam(value = "id") Integer courseId) {
        CourseResponse course = courseService.detailFront(courseId);
        return CommonResult.success(course);
    }
    @ApiOperation(value = "章节详情")
    @RequestMapping(value = "/courseListInfo", method = RequestMethod.GET)
    public CommonResult<CourseListResponse> courseListInfo(@RequestParam(value = "courseListId") Integer courseListId) {
        CourseListResponse courseListResponse = courseListService.detailFront(courseListId);
        return CommonResult.success(courseListResponse);
    }

    /**
     * 增加播放量
     *
     * @param courseId Integer
     * @param courseListId Integer
     * @since 2022-07-22
     */
    @ApiOperation(value = "增加播放量")
    @RequestMapping(value = "/addPlayTime", method = RequestMethod.GET)
    public CommonResult<String> addPlayTime(@RequestParam(value = "courseId") Integer courseId, @RequestParam(value = "courseListId") Integer courseListId) {
        return courseService.addPlayTime(courseId,courseListId);
    }
}



