package com.zbkj.front.controller.course;

import com.zbkj.common.model.course.StoreOrderMember;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.StoreOrderMemberRequest;
import com.zbkj.common.request.course.StoreOrderMemberSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.StoreOrderMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/admin/store-order-member")
@Api(tags = "") //配合swagger使用

public class StoreOrderMemberController {

    @Autowired
    private StoreOrderMemberService storeOrderMemberService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-09-21
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<StoreOrderMember>>  getList(@Validated StoreOrderMemberSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<StoreOrderMember> storeOrderMemberCommonPage = CommonPage.restPage(storeOrderMemberService.getList(request, pageParamRequest));
        return CommonResult.success(storeOrderMemberCommonPage);
    }

    /**
     * 新增
     * @param storeOrderMemberRequest 新增参数
     * @author Mr.guqianbin
     * @since 2022-09-21
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@Validated StoreOrderMemberRequest storeOrderMemberRequest){
        StoreOrderMember storeOrderMember = new StoreOrderMember();
        BeanUtils.copyProperties(storeOrderMemberRequest, storeOrderMember);

        if(storeOrderMemberService.save(storeOrderMember)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-09-21
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(storeOrderMemberService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param id integer id
     * @param storeOrderMemberRequest 修改参数
     * @author Mr.guqianbin
     * @since 2022-09-21
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestParam Integer id, @Validated StoreOrderMemberRequest storeOrderMemberRequest){
        StoreOrderMember storeOrderMember = new StoreOrderMember();
        BeanUtils.copyProperties(storeOrderMemberRequest, storeOrderMember);
        storeOrderMember.setId(id);

        if(storeOrderMemberService.updateById(storeOrderMember)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-09-21
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<StoreOrderMember> info(@RequestParam(value = "id") Integer id){
        StoreOrderMember storeOrderMember = storeOrderMemberService.getById(id);
        return CommonResult.success(storeOrderMember);
   }
}



