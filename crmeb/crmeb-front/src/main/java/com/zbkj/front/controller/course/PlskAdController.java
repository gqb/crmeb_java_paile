package com.zbkj.front.controller.course;

import com.zbkj.common.model.course.PlskAd;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.PlskAdRequest;
import com.zbkj.common.request.course.PlskAdSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.PlskAdService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/plsk-ad")
@Api(tags = "派乐时刻") //配合swagger使用

public class PlskAdController {

    @Autowired
    private PlskAdService plskAdService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-09-04
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<PlskAd>> getList(@Validated PlskAdSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<PlskAd> plskAdCommonPage = CommonPage.restPage(plskAdService.getList(request, pageParamRequest));
        return CommonResult.success(plskAdCommonPage);
    }


}



