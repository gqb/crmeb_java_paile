package com.zbkj.front.controller.course;

import com.zbkj.common.model.course.CourseProduction;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionRequest;
import com.zbkj.common.request.course.CourseProductionSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseProductionResponse;
import com.zbkj.service.service.course.CourseProductionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/course-production")
@Api(tags = "课程-课程作品") //配合swagger使用

public class CourseProductionController {

    @Autowired
    private CourseProductionService courseProductionService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CourseProductionResponse>> getList(@Validated CourseProductionSearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<CourseProductionResponse> courseProductionCommonPage = CommonPage.restPage(courseProductionService.getList(request, pageParamRequest));
        return CommonResult.success(courseProductionCommonPage);
    }
    @ApiOperation(value = "作品详情") //配合swagger使用
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<CourseProductionResponse> info(@RequestParam(value = "productionId") Integer productionId){
        return CommonResult.success(courseProductionService.info(productionId));
    }
    /**
     * 新增
     * @param courseProductionRequest 新增参数
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "新增课程作品")
    @RequestMapping(value = "/uploadProduction", method = RequestMethod.POST)
    public CommonResult<String> uploadProduction(@RequestBody @Validated CourseProductionRequest courseProductionRequest){
       return courseProductionService.uploadProduction(courseProductionRequest);
    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @ApiOperation(value = "删除作品，会判断是否是自己的作品")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        return courseProductionService.deleteProduction(id);
    }

}



