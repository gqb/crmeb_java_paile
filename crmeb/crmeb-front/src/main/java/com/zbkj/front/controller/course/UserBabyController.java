package com.zbkj.front.controller.course;

import com.zbkj.common.model.course.UserBaby;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserBabyRequest;
import com.zbkj.common.request.course.UserBabySearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.course.UserBabyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 *  前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/user-baby")
@Api(tags = "用户-宝贝") //配合swagger使用

public class UserBabyController {

    @Autowired
    private UserBabyService userBabyService;

    /**
     * 分页显示
     * @param request 搜索条件
     * @param pageParamRequest 分页参数
     * @author Mr.guqianbin
     * @since 2022-08-09
     */
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<UserBaby>> getList(@Validated UserBabySearchRequest request, @Validated PageParamRequest pageParamRequest){
        CommonPage<UserBaby> userBabyCommonPage = CommonPage.restPage(userBabyService.getList(request, pageParamRequest));
        return CommonResult.success(userBabyCommonPage);
    }

    /**
     * 新增
     * @param userBabyRequest 新增参数
     * @author Mr.guqianbin
     * @since 2022-08-09
     */
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@RequestBody @Validated UserBabyRequest userBabyRequest) throws Exception {
        return userBabyService.addBaby(userBabyRequest);

    }

    /**
     * 删除
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-09
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id){
        if(userBabyService.removeById(id)){
            return CommonResult.success();
        }else{
            return CommonResult.failed();
        }
    }

    /**
     * 修改
     * @param userBabyRequest 修改参数
     * @author Mr.guqianbin
     * @since 2022-08-09
     */
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestBody @Validated UserBabyRequest userBabyRequest) throws Exception {
        return userBabyService.updateBaby(userBabyRequest);

    }

    /**
     * 查询信息
     * @param id Integer
     * @author Mr.guqianbin
     * @since 2022-08-09
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<UserBaby> info(@RequestParam(value = "id") Integer id){
        UserBaby userBaby = userBabyService.getById(id);
        return CommonResult.success(userBaby);
   }
}



