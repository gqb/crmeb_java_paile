package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.CourseLabel;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseLabelSearchRequest;
import com.zbkj.service.dao.CourseLabelDao;
import com.zbkj.service.service.course.CourseLabelService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.guqianbin
* @description CourseLabelServiceImpl 接口实现
* @date 2022-08-10
*/
@Service
public class CourseLabelServiceImpl extends ServiceImpl<CourseLabelDao, CourseLabel> implements CourseLabelService {

    @Resource
    private CourseLabelDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.guqianbin
    * @since 2022-08-10
    * @return List<CourseLabel>
    */
    @Override
    public List<CourseLabel> getList(CourseLabelSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseLabel 类的多条件查询
        LambdaQueryWrapper<CourseLabel> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseLabel model = new CourseLabel();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

    @Override
    public List<CourseLabel> getLabelByCourseId(Integer courseId) {
        LambdaQueryWrapper<CourseLabel> lqw=new LambdaQueryWrapper<>();
        lqw.eq(CourseLabel::getCourseId,courseId);
        List<CourseLabel> courseLabels=list(lqw);
        return courseLabels;
    }

    @Override
    public List<CourseLabel> getLabelByCourseIds(List<Integer> courseIds) {
        LambdaQueryWrapper<CourseLabel> lqw=new LambdaQueryWrapper<>();
        lqw.in(CourseLabel::getCourseId,courseIds);
        List<CourseLabel> courseLabels=list(lqw);
        return courseLabels;
    }

}

