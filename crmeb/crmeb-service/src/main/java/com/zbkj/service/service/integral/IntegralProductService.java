package com.zbkj.service.service.integral;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.integral.IntegralProduct;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.StoreProductStockRequest;
import com.zbkj.common.request.integral.IntegralProductRequest;
import com.zbkj.common.request.integral.IntegralProductSearchRequest;
import com.zbkj.common.response.integral.IntegralProductDetailH5Response;
import com.zbkj.common.response.integral.IntegralProductDetailResponse;

import java.util.List;

/**
* @author Mr.Zhang
* @description IntegralProductService 接口
* @date 2021-07-20
*/
public interface IntegralProductService extends IService<IntegralProduct> {

    List<IntegralProduct> getList(IntegralProductSearchRequest request, PageParamRequest pageParamRequest);

    Boolean addProduct(IntegralProductRequest productRequest);

    Boolean operationStock(Integer integralProductId,Integer num,String type);

    IntegralProductDetailResponse detail(Integer integralProductId);
    IntegralProductDetailH5Response detailH5(Integer integralProductId);

    Boolean stockAddRedis(StoreProductStockRequest stockRequest);

    boolean updateShow(Integer integralProductId,boolean isShow);
    boolean update(IntegralProductRequest productRequest);

    void consumeProductStock();
}