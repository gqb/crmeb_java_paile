package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.ActivityLocation;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.ActivityLocationSearchRequest;
import com.zbkj.service.dao.ActivityLocationDao;
import com.zbkj.service.service.course.ActivityLocationService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.guqianbin
* @description ActivityLocationServiceImpl 接口实现
* @date 2022-09-29
*/
@Service
public class ActivityLocationServiceImpl extends ServiceImpl<ActivityLocationDao, ActivityLocation> implements ActivityLocationService {

    @Resource
    private ActivityLocationDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.guqianbin
    * @since 2022-09-29
    * @return List<ActivityLocation>
    */
    @Override
    public List<ActivityLocation> getList(ActivityLocationSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 ActivityLocation 类的多条件查询
        LambdaQueryWrapper<ActivityLocation> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        ActivityLocation model = new ActivityLocation();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

