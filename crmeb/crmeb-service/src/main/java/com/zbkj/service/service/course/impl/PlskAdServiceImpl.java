package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.PlskAd;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.PlskAdSearchRequest;
import com.zbkj.service.dao.PlskAdDao;
import com.zbkj.service.service.course.PlskAdService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.guqianbin
* @description PlskAdServiceImpl 接口实现
* @date 2022-09-04
*/
@Service
public class PlskAdServiceImpl extends ServiceImpl<PlskAdDao, PlskAd> implements PlskAdService {

    @Resource
    private PlskAdDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.guqianbin
    * @since 2022-09-04
    * @return List<PlskAd>
    */
    @Override
    public List<PlskAd> getList(PlskAdSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 PlskAd 类的多条件查询
        LambdaQueryWrapper<PlskAd> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        PlskAd model = new PlskAd();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        lambdaQueryWrapper.orderByDesc(PlskAd::getSort);
        return dao.selectList(lambdaQueryWrapper);
    }

}

