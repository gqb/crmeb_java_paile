package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseList;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseListRequest;
import com.zbkj.common.request.course.CourseListSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseListResponse;
import com.zbkj.common.response.course.CourseListStudyLogResponse;

import java.util.List;

/**
* @author Mr.Zhang
* @description CourseListService 接口
* @date 2022-07-22
*/
public interface CourseListService extends IService<CourseList> {

    List<CourseList> getList(CourseListSearchRequest request, PageParamRequest pageParamRequest);
    List<CourseList> getListFront(CourseListSearchRequest request, PageParamRequest pageParamRequest);
    CommonResult<String> add(CourseListRequest request);
    CommonResult<String> update(CourseListRequest request);

    CourseListResponse detailFront(Integer courseListId);
    CourseListResponse detail(Integer courseListId);

    boolean delete(Integer courseListId);

}