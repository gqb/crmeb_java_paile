package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.StoreOrderMember;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.StoreOrderMemberSearchRequest;

import java.util.List;

/**
* @author Mr.guqianbin
* @description StoreOrderMemberService 接口
* @date 2022-09-21
*/
public interface StoreOrderMemberService extends IService<StoreOrderMember> {

    List<StoreOrderMember> getList(StoreOrderMemberSearchRequest request, PageParamRequest pageParamRequest);
}