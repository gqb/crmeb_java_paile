package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.course.ActivityLocation;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-09-29
 */
public interface ActivityLocationDao extends BaseMapper<ActivityLocation> {

}
