package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.course.UserTaskLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-14
 */
public interface UserTaskLogDao extends BaseMapper<UserTaskLog> {

}
