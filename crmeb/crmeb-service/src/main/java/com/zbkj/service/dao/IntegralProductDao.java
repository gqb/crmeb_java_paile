package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.integral.IntegralProduct;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.Zhang
 * @since 2021-07-20
 */
public interface IntegralProductDao extends BaseMapper<IntegralProduct> {

}
