package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.StoreOrderMember;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.StoreOrderMemberSearchRequest;
import com.zbkj.service.dao.StoreOrderMemberDao;
import com.zbkj.service.service.course.StoreOrderMemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.guqianbin
* @description StoreOrderMemberServiceImpl 接口实现
* @date 2022-09-21
*/
@Service
public class StoreOrderMemberServiceImpl extends ServiceImpl<StoreOrderMemberDao, StoreOrderMember> implements StoreOrderMemberService {

    @Resource
    private StoreOrderMemberDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.guqianbin
    * @since 2022-09-21
    * @return List<StoreOrderMember>
    */
    @Override
    public List<StoreOrderMember> getList(StoreOrderMemberSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 StoreOrderMember 类的多条件查询
        LambdaQueryWrapper<StoreOrderMember> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        StoreOrderMember model = new StoreOrderMember();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

