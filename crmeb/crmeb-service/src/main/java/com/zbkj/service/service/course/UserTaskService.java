package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.UserTask;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserTaskSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.UserTaskResponse;

import java.util.List;

/**
* @author Mr.guqianbin
* @description UserTaskService 接口
* @date 2022-08-14
*/
public interface UserTaskService extends IService<UserTask> {

    List<UserTask> getList(UserTaskSearchRequest request, PageParamRequest pageParamRequest);

    List<UserTaskResponse> getTodayTask();

    CommonResult<Boolean> completeTask(Integer taskId);

    /**
     * 用户点赞积分任务
     */
    void likeTaskComplete(Integer productionId);

    void publicProductionTask(Integer uid);
}