package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.Activity;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.ActivityRequest;
import com.zbkj.common.request.course.ActivitySearchRequest;
import com.zbkj.common.response.course.ActivityResponse;

import java.util.List;
import java.util.Map;

/**
* @author Mr.guqianbin
* @description ActivityService 接口
* @date 2022-08-31
*/
public interface ActivityService extends IService<Activity> {

    List<ActivityResponse> getList(ActivitySearchRequest request, PageParamRequest pageParamRequest);
    ActivityResponse detail(Integer id);

    Map<String,Object> add(ActivityRequest activityRequest);
    Map<String,Object> update(ActivityRequest activityRequest);
}