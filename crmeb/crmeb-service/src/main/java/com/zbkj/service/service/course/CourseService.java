package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.course.Course;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseRequest;
import com.zbkj.common.request.course.CourseSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseResponse;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
* @author Mr.Zhang
* @description CourseService 接口
* @date 2022-07-22
*/
public interface CourseService extends IService<Course> {

    PageInfo<CourseResponse> getList(CourseSearchRequest request, PageParamRequest pageParamRequest);
    PageInfo<CourseResponse> getListFront(CourseSearchRequest request, PageParamRequest pageParamRequest);

    CommonResult<String> addCourse(CourseRequest request);
    CommonResult<Map<String,Object>> updateCourse(CourseRequest request);
    CommonResult<Map<String,Object>> onlineOff(Integer id,Boolean isShow);

    CourseResponse detail(Integer id);
    CourseResponse detailFront(Integer id);

    CommonResult<String> deleteCourse(Integer id,String type);

    CommonResult<String> addPlayTime(Integer courseId,Integer courseListId);



}