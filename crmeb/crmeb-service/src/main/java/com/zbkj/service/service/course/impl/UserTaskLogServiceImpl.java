package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.UserTaskLog;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserTaskLogSearchRequest;
import com.zbkj.service.dao.UserTaskLogDao;
import com.zbkj.service.service.course.UserTaskLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.guqianbin
* @description UserTaskLogServiceImpl 接口实现
* @date 2022-08-14
*/
@Service
public class UserTaskLogServiceImpl extends ServiceImpl<UserTaskLogDao, UserTaskLog> implements UserTaskLogService {

    @Resource
    private UserTaskLogDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.guqianbin
    * @since 2022-08-14
    * @return List<UserTaskLog>
    */
    @Override
    public List<UserTaskLog> getList(UserTaskLogSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 UserTaskLog 类的多条件查询
        LambdaQueryWrapper<UserTaskLog> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        UserTaskLog model = new UserTaskLog();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

