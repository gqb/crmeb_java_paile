package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.UserBaby;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserBabyRequest;
import com.zbkj.common.request.course.UserBabySearchRequest;
import com.zbkj.common.response.CommonResult;

import java.util.List;

/**
* @author Mr.guqianbin
* @description UserBabyService 接口
* @date 2022-08-09
*/
public interface UserBabyService extends IService<UserBaby> {

    List<UserBaby> getList(UserBabySearchRequest request, PageParamRequest pageParamRequest);

    CommonResult<String> addBaby(UserBabyRequest request) throws Exception;
    CommonResult<String> updateBaby(UserBabyRequest request) throws Exception;
}