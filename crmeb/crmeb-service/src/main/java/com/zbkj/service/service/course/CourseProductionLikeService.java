package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseProductionLike;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionLikeRequest;
import com.zbkj.common.request.course.CourseProductionLikeSearchRequest;
import com.zbkj.common.response.CommonResult;

import java.util.List;

/**
* @author Mr.guqianbin
* @description CourseProductionLikeService 接口
* @date 2022-08-02
*/
public interface CourseProductionLikeService extends IService<CourseProductionLike> {

    List<CourseProductionLike> getList(CourseProductionLikeSearchRequest request, PageParamRequest pageParamRequest);

    CommonResult<String> like(CourseProductionLikeRequest request);

}