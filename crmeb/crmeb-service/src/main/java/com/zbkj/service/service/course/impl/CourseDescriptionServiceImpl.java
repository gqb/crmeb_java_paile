package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.CourseDescription;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseDescriptionSearchRequest;
import com.zbkj.service.dao.CourseDescriptionDao;
import com.zbkj.service.service.course.CourseDescriptionService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.guqianbin
* @description CourseDescriptionServiceImpl 接口实现
* @date 2022-08-10
*/
@Service
public class CourseDescriptionServiceImpl extends ServiceImpl<CourseDescriptionDao, CourseDescription> implements CourseDescriptionService {

    @Resource
    private CourseDescriptionDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.guqianbin
    * @since 2022-08-10
    * @return List<CourseDescription>
    */
    @Override
    public List<CourseDescription> getList(CourseDescriptionSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseDescription 类的多条件查询
        LambdaQueryWrapper<CourseDescription> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseDescription model = new CourseDescription();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

    @Override
    public boolean deleteById(Integer type, Integer linkId) {
        LambdaQueryWrapper<CourseDescription> lqw=new LambdaQueryWrapper<>();
        lqw.eq(CourseDescription::getType,type);
        lqw.eq(CourseDescription::getLinkId,linkId);
        return remove(lqw);
    }

}

