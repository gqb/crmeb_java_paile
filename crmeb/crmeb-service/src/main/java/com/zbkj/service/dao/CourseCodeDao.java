package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.course.CourseCode;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.Zhang
 * @since 2022-07-22
 */
public interface CourseCodeDao extends BaseMapper<CourseCode> {

}
