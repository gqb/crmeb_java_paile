package com.zbkj.service.service.course.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.Course;
import com.zbkj.common.model.course.CourseProduction;
import com.zbkj.common.model.course.CourseProductionLike;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionLikeRequest;
import com.zbkj.common.request.course.CourseProductionLikeSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.dao.CourseProductionLikeDao;
import com.zbkj.service.service.UserService;
import com.zbkj.service.service.course.CourseProductionLikeService;
import com.zbkj.service.service.course.CourseProductionService;
import com.zbkj.service.service.course.CourseService;
import com.zbkj.service.service.course.UserTaskService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
* @author Mr.guqianbin
* @description CourseProductionLikeServiceImpl 接口实现
* @date 2022-08-02
*/
@Service
public class CourseProductionLikeServiceImpl extends ServiceImpl<CourseProductionLikeDao, CourseProductionLike> implements CourseProductionLikeService {

    @Resource
    private CourseProductionLikeDao dao;

    @Autowired
    private UserService userService;


    @Autowired
    private CourseProductionService courseProductionService;
    @Autowired
    private UserTaskService userTaskService;
    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.guqianbin
    * @since 2022-08-02
    * @return List<CourseProductionLike>
    */
    @Override
    public List<CourseProductionLike> getList(CourseProductionLikeSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseProductionLike 类的多条件查询
        LambdaQueryWrapper<CourseProductionLike> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseProductionLike model = new CourseProductionLike();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

    @Override
    @Transactional
    public CommonResult<String> like(CourseProductionLikeRequest request) {
        Integer uid=userService.getUserIdException();

        LambdaQueryWrapper<CourseProductionLike> lqw=new LambdaQueryWrapper<>();
        lqw.eq(CourseProductionLike::getCourseProductionId,request.getCourseProductionId());
        lqw.eq(CourseProductionLike::getUid,uid);
        List<CourseProductionLike> likeList=list(lqw);
        if(CollectionUtil.isEmpty(likeList)){//空表示要点赞
            CourseProductionLike like=new CourseProductionLike();
            like.setUid(uid);
            like.setCourseProductionId(request.getCourseProductionId());
            like.setCreateTime(new Date());
            save(like);
            LambdaUpdateWrapper<CourseProduction> luw=new LambdaUpdateWrapper<>();
            luw.setSql(StrUtil.format("like_count = like_count + {}",1));
            luw.eq(CourseProduction::getId,request.getCourseProductionId());
            courseProductionService.update(luw);
            // 增加点赞任务积分
            userTaskService.likeTaskComplete(request.getCourseProductionId());
            return CommonResult.success("点赞成功");
        }else{//已经点过赞，再点表示取消点赞
            removeById(likeList.get(0).getId());
            LambdaUpdateWrapper<CourseProduction> luw=new LambdaUpdateWrapper<>();
            luw.setSql(StrUtil.format("like_count = like_count - {}",1));
            luw.eq(CourseProduction::getId,request.getCourseProductionId());
            courseProductionService.update(luw);
            return CommonResult.success("取消点赞");
        }



    }


}

