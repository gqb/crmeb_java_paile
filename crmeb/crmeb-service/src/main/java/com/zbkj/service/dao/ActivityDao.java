package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.course.Activity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-31
 */
public interface ActivityDao extends BaseMapper<Activity> {

}
