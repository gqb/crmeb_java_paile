package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.PlskAd;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.PlskAdSearchRequest;

import java.util.List;

/**
* @author Mr.guqianbin
* @description PlskAdService 接口
* @date 2022-09-04
*/
public interface PlskAdService extends IService<PlskAd> {

    List<PlskAd> getList(PlskAdSearchRequest request, PageParamRequest pageParamRequest);


}