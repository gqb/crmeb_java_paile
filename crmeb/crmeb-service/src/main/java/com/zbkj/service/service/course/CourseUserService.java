package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.course.CourseUser;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseUserSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseResponse;
import com.zbkj.common.response.course.CourseUserResponse;

import java.util.List;

/**
* @author Mr.Zhang
* @description CourseUserService 接口
* @date 2022-07-26
*/
public interface CourseUserService extends IService<CourseUser> {

    PageInfo<CourseUserResponse> getList(CourseUserSearchRequest request, PageParamRequest pageParamRequest);

    CommonResult<String> exchangeCourse(String courseCode, Integer courseId);

    CourseResponse detailFront(Integer id);
}