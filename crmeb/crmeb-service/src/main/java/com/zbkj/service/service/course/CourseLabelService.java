package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseLabel;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseLabelSearchRequest;

import java.util.List;

/**
* @author Mr.guqianbin
* @description CourseLabelService 接口
* @date 2022-08-10
*/
public interface CourseLabelService extends IService<CourseLabel> {

    List<CourseLabel> getList(CourseLabelSearchRequest request, PageParamRequest pageParamRequest);


    List<CourseLabel> getLabelByCourseId(Integer courseId);
    List<CourseLabel> getLabelByCourseIds(List<Integer> courseIds);
}