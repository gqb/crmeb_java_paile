package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseDescription;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseDescriptionSearchRequest;

import java.util.List;

/**
* @author Mr.guqianbin
* @description CourseDescriptionService 接口
* @date 2022-08-10
*/
public interface CourseDescriptionService extends IService<CourseDescription> {

    List<CourseDescription> getList(CourseDescriptionSearchRequest request, PageParamRequest pageParamRequest);

    boolean deleteById(Integer type,Integer linkId);
}