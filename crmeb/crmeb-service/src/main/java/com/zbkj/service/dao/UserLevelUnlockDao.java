package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.course.UserLevelUnlock;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-13
 */
public interface UserLevelUnlockDao extends BaseMapper<UserLevelUnlock> {

}
