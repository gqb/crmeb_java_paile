package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.ActivityLog;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.ActivityLogSearchRequest;
import com.zbkj.common.request.course.QRCodeSweepRequest;

import java.util.List;
import java.util.Map;

/**
* @author Mr.guqianbin
* @description ActivityLogService 接口
* @date 2022-08-31
*/
public interface ActivityLogService extends IService<ActivityLog> {

    List<ActivityLog> getList(ActivityLogSearchRequest request, PageParamRequest pageParamRequest);
    /**
     * 互动二维码签到
     * @return
     */
    Map<String,Object> activitySign(QRCodeSweepRequest request);
}