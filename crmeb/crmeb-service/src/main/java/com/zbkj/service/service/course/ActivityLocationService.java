package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.ActivityLocation;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.ActivityLocationSearchRequest;

import java.util.List;

/**
* @author Mr.guqianbin
* @description ActivityLocationService 接口
* @date 2022-09-29
*/
public interface ActivityLocationService extends IService<ActivityLocation> {

    List<ActivityLocation> getList(ActivityLocationSearchRequest request, PageParamRequest pageParamRequest);
}