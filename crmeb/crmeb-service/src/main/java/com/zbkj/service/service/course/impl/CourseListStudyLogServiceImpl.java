package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.CourseListStudyLog;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseListStudyLogSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.dao.CourseListStudyLogDao;
import com.zbkj.service.service.UserService;
import com.zbkj.service.service.course.CourseListStudyLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * @author Mr.Zhang
 * @description CourseListStudyLogServiceImpl 接口实现
 * @date 2022-07-22
 */
@Service
public class CourseListStudyLogServiceImpl extends ServiceImpl<CourseListStudyLogDao, CourseListStudyLog> implements CourseListStudyLogService {

    @Resource
    private CourseListStudyLogDao dao;
    @Autowired
    private UserService userService;

    /**
     * 列表
     *
     * @param request          请求参数
     * @param pageParamRequest 分页类参数
     * @return List<CourseListStudyLog>
     * @author Mr.Zhang
     * @since 2022-07-22
     */
    @Override
    public List<CourseListStudyLog> getList(CourseListStudyLogSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 CourseListStudyLog 类的多条件查询
        LambdaQueryWrapper<CourseListStudyLog> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        CourseListStudyLog model = new CourseListStudyLog();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

    /**
     * 课程学习进度更新 接口，用来更新学习到第几分钟
     *
     * @param courseListStudyLogId
     * @param studyLength
     * @return
     */
    @Override
    public CommonResult<String> updateProgress(Integer courseListStudyLogId, Integer studyLength) {
        Integer userId = userService.getUserIdException();
        CourseListStudyLog courseListStudyLog=getById(courseListStudyLogId);
        BigDecimal study=new BigDecimal(studyLength);
        BigDecimal length=new BigDecimal(courseListStudyLog.getVideoLength());

        BigDecimal remain=study.divide(length,2, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
        LambdaUpdateWrapper<CourseListStudyLog> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(CourseListStudyLog::getStatus,1);
        if(remain.compareTo(new BigDecimal(95))>=0){
            updateWrapper.set(CourseListStudyLog::getStatus,2);
        }
        if(length.compareTo(study)<0){
            studyLength=study.intValue();
        }
        updateWrapper.set(CourseListStudyLog::getStudyLength,studyLength);
        updateWrapper.eq(CourseListStudyLog::getId,courseListStudyLogId);
        updateWrapper.eq(CourseListStudyLog::getUid,userId);
        boolean result=update(updateWrapper);
        if(result){
           return  CommonResult.success();
        }else{
           return   CommonResult.failed();
        }
    }
}

