package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.course.CourseProduction;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionRequest;
import com.zbkj.common.request.course.CourseProductionSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.course.CourseProductionResponse;

import java.util.List;

/**
* @author Mr.Zhang
* @description CourseProductionService 接口
* @date 2022-07-22
*/
public interface CourseProductionService extends IService<CourseProduction> {

    PageInfo<CourseProductionResponse> getList(CourseProductionSearchRequest request, PageParamRequest pageParamRequest);

    CommonResult<String> uploadProduction(CourseProductionRequest request);
    CourseProductionResponse info(Integer productionId);


    CommonResult<String> deleteProduction(Integer courseProductionId);
}