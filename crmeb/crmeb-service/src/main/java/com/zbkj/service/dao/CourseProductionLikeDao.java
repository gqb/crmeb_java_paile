package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.course.CourseProductionLike;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mr.guqianbin
 * @since 2022-08-02
 */
public interface CourseProductionLikeDao extends BaseMapper<CourseProductionLike> {

}
