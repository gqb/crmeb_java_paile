package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.UserTaskLog;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserTaskLogSearchRequest;

import java.util.List;

/**
* @author Mr.guqianbin
* @description UserTaskLogService 接口
* @date 2022-08-14
*/
public interface UserTaskLogService extends IService<UserTaskLog> {

    List<UserTaskLog> getList(UserTaskLogSearchRequest request, PageParamRequest pageParamRequest);
}