package com.zbkj.service.service.course.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.UserLevelUnlock;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserLevelUnlockSearchRequest;
import com.zbkj.service.dao.UserLevelUnlockDao;
import com.zbkj.service.service.course.UserLevelUnlockService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.guqianbin
* @description UserLevelUnlockServiceImpl 接口实现
* @date 2022-08-13
*/
@Service
public class UserLevelUnlockServiceImpl extends ServiceImpl<UserLevelUnlockDao, UserLevelUnlock> implements UserLevelUnlockService {

    @Resource
    private UserLevelUnlockDao dao;


    /**
    * 列表
    * @param request 请求参数
    * @param pageParamRequest 分页类参数
    * @author Mr.guqianbin
    * @since 2022-08-13
    * @return List<UserLevelUnlock>
    */
    @Override
    public List<UserLevelUnlock> getList(UserLevelUnlockSearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 UserLevelUnlock 类的多条件查询
        LambdaQueryWrapper<UserLevelUnlock> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        UserLevelUnlock model = new UserLevelUnlock();
        BeanUtils.copyProperties(request, model);
        lambdaQueryWrapper.setEntity(model);
        return dao.selectList(lambdaQueryWrapper);
    }

}

