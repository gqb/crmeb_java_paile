package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseProductionImg;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseProductionImgSearchRequest;

import java.util.List;

/**
* @author Mr.Zhang
* @description CourseProductionImgService 接口
* @date 2022-07-22
*/
public interface CourseProductionImgService extends IService<CourseProductionImg> {

    List<CourseProductionImg> getList(CourseProductionImgSearchRequest request, PageParamRequest pageParamRequest);
}