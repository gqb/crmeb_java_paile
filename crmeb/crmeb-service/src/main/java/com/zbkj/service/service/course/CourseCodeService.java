package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.CourseCode;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.CourseCodeSearchRequest;
import com.zbkj.common.request.course.QRCodeSweepRequest;
import com.zbkj.common.response.course.CourseCodeResponse;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author Mr.Zhang
* @description CourseCodeService 接口
* @date 2022-07-22
*/
public interface CourseCodeService extends IService<CourseCode> {

    List<CourseCodeResponse> getList(CourseCodeSearchRequest request, PageParamRequest pageParamRequest);
    String generateCode(Integer courseId,Integer exchangeCount);

    List<String> generateCourseIntegralQRCode(Integer courseId,int count,int integral) throws IOException;


    String exchangeIntegralCode(String integralQRCode);

    String generateActivityQRCode(Integer linkedId, String beginDate,String endDate,Integer integral);


    Map<String,Object> sweepQRCode(QRCodeSweepRequest request);

}