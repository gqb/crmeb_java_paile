package com.zbkj.service.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.course.UserLevelUnlock;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.UserLevelUnlockSearchRequest;

import java.util.List;

/**
* @author Mr.guqianbin
* @description UserLevelUnlockService 接口
* @date 2022-08-13
*/
public interface UserLevelUnlockService extends IService<UserLevelUnlock> {

    List<UserLevelUnlock> getList(UserLevelUnlockSearchRequest request, PageParamRequest pageParamRequest);
}