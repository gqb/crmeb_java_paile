package com.zbkj.service.service.course.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.course.Activity;
import com.zbkj.common.model.course.ActivityLog;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.course.ActivityRequest;
import com.zbkj.common.request.course.ActivitySearchRequest;
import com.zbkj.common.response.course.ActivityResponse;
import com.zbkj.common.utils.QRCodeUtil;
import com.zbkj.service.dao.ActivityDao;
import com.zbkj.service.service.course.ActivityLogService;
import com.zbkj.service.service.course.ActivityService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

/**
 * @author Mr.guqianbin
 * @description ActivityServiceImpl 接口实现
 * @date 2022-08-31
 */
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityDao, Activity> implements ActivityService {

    private static final Logger log = LoggerFactory.getLogger(ActivityServiceImpl.class);

    @Resource
    private ActivityDao dao;

    @Autowired
    private ActivityLogService activityLogService;

    /**
     * 列表
     *
     * @param request          请求参数
     * @param pageParamRequest 分页类参数
     * @return List<Activity>
     * @author Mr.guqianbin
     * @since 2022-08-31
     */
    @Override
    public List<ActivityResponse> getList(ActivitySearchRequest request, PageParamRequest pageParamRequest) {
        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());

        //带 Activity 类的多条件查询
        LambdaQueryWrapper<Activity> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotEmpty(request.getName())){
            lambdaQueryWrapper.like(Activity::getName,request.getName());
        }
        List<Activity> activities=dao.selectList(lambdaQueryWrapper);
        List<ActivityResponse> activityResponses=new ArrayList<>();
        if(CollectionUtil.isNotEmpty(activities)){
            activities.forEach(activity -> {
                ActivityResponse activityResponse=new ActivityResponse();
                BeanUtil.copyProperties(activity,activityResponse);
                //如果是积分二维码 就生成base64的编码
                if (ObjectUtil.isNotEmpty(activity.getCode())) {
                    try {
                        activityResponse.setCodeString(QRCodeUtil.crateQRCode(activityResponse.getCode(), 1024, 1024));
                    } catch (IOException e) {
                        log.info("生成二维码报错");
                        throw new RuntimeException(e);
                    }
                }
                activityResponses.add(activityResponse);
            });
        }
        return activityResponses;
    }

    @Override
    public ActivityResponse detail(Integer id) {
        ActivityResponse response=new ActivityResponse();
        Activity activity = getById(id);
        if(ObjectUtil.isEmpty(activity)){
            return response;
        }
        BeanUtil.copyProperties(activity,response);
        LambdaQueryWrapper<ActivityLog> lqw=new LambdaQueryWrapper<>();
        lqw.eq(ActivityLog::getActivityId,id);
        List<ActivityLog> logs=activityLogService.list(lqw);
        if(CollectionUtil.isNotEmpty(logs)){
            response.setActivityLogs(logs);
        }
        return response;
    }

    @Override
    public Map<String, Object> add(ActivityRequest activityRequest) {
        Map<String, Object> data = new HashMap<>();
        Activity activity = new Activity();
        BeanUtils.copyProperties(activityRequest, activity);
//        Date beginDate = DateUtil.parse(activityRequest.getBeginDate());
//        Date endDate = DateUtil.parse(activityRequest.getEndDate());
//        if (beginDate.compareTo(endDate) >= 0) {
//            data.put("success", false);
//            data.put("message", "开始时间不能大于结束时间");
//            return data;
//        }
        activity.setId(null);
        activity.setCode("plsk_qd"+
                DateUtil.format(new Date(),"yyyy-MM-dd")+"-"
                +activity.getCodeValue()+"-"+CourseCodeServiceImpl.createcode(8));
//        activity.setBeginDate(beginDate);
//        activity.setEndDate(endDate);
        save(activity);
        data.put("success", true);
        data.put("message", "添加成功");
        return data;
    }

    @Override
    public Map<String, Object> update( ActivityRequest activityRequest) {
        Map<String, Object> data = new HashMap<>();
        Activity activity = getById(activityRequest.getId());
//        BeanUtils.copyProperties(activityRequest, activity);
        BeanUtil.copyProperties(activityRequest, activity, CopyOptions.create().ignoreNullValue());

        updateById(activity);
        data.put("success", true);
        data.put("message", "更新成功");
        return data;
    }

}

