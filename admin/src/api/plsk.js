import request from '@/utils/request'

/**
 * 派乐时刻列表
 * 接口地址 /plskadmin/api/admin/plsk-ad/list
 * @param pram
 */
export function plskLstApi(params) {
  return request({
    url: '/admin/plsk-ad/list',
    method: 'GET',
    params
  })
}
/**
 * 派乐时刻详情 接口地址 /plskadmin/api/admin/plsk-ad/info
 * */
export function plskInfoApi(data) {
  return request({
    url: `/admin/plsk-ad/info`,
    method: 'get',
    params:data
  })
}
/**
 * 新增章节 接口地址 /plskadmin/api/admin/plsk-ad/save
 * */
export function addPlskApi(data) {
  return request({
    url: '/admin/plsk-ad/save',
    method: 'POST',
    param:data,
    data
  })
}
/**
 * 修改章节 接口地址 /plskadmin/api/admin/plsk-ad/update
 * */
export function updatePlskApi(data) {
  return request({
    url: '/admin/plsk-ad/update',
    method: 'POST',
    data
  })
}

/**
 * 接口地址 /plskadmin/api/admin/plsk-ad/delete
 * */
export function deletePlskApi(data) {
  return request({
    url: '/admin/plsk-ad/delete',
    method: 'DELETE',
    params:data,
  })
}
