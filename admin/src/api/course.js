// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import request from '@/utils/request'

export function addCourseApi(data) {
  return request({
    url: '/admin/course/save',
    method: 'POST',
    data
  })
}
/**
 * 课程列表
 * @param pram
 */
export function courseLstApi(params) {
  return request({
    url: '/admin/course/list',
    method: 'GET',
    params
  })
}
/**
 * 课程分类
 * @param pram
 */
export function categoryApi(params) {
  return request({
    url: '/admin/category/list/tree',
    method: 'GET',
    params
  })
}
/**
 * 删除商品
 * @param pram
 */
export function courseDeleteApi(id, type) {
  return request({
    url: `/admin/course/delete/${id}`,
    method: 'get',
    params:{type:type}
  })
}
/**
 * 上架、下架课程
 * @param pram
 */
export function upAndDownCourseApi(data) {
  return request({
    url: '/admin/course/update',
    method: 'POST',
    data
  })
}
/**
 * 接口地址 /plskadmin/api/admin/course/onlineOff
 * */
export function onlineOffApi(data) {
  return request({
    url: '/admin/course/onlineOff',
    method: 'POST',
    data
  })
}

/**
 * 上架、下架课程
 * @param pram
 */
export function recycleCourseApi(data) {
  return request({
    url: '/admin/course/update',
    method: 'POST',
    params:data,
    data
  })
}
/**
 * 课程详情 admin/course/info
 * */
export function courseInfoApi(data) {
  return request({
    url: `/admin/course/info`,
    method: 'get',
    params:data
  })
}
/**
 * 课程码分页列表 接口地址 /plskadmin/api/admin/course-code/list
 * */
export function getCourseCodeListApi(data) {
  return request({
    url: `/admin/course-code/list`,
    method: 'get',
    params:data
  })
}
/**
 * 生产课程码 接口地址 /plskadmin/api/admin/course-code/generateCode
 * */
export function generateCodeApi(data) {
  return request({
    url: '/admin/course-code/generateCode',
    method: 'POST',
    params:data,
    data
  })
}

/**
 * 生产积分二维码 接口地址 /plskadmin/api/admin/course-code/generateQRCode
 * */
export function generateQRCodeApi(data) {
  return request({
    url: '/admin/course-code/generateQRCode',
    method: 'POST',
    params:data,
    data
  })
}

/**
 * 章节列表 接口地址 /plskadmin/api/admin/course-list/list
 * */
export function getCourseListListApi(data) {
  return request({
    url: `/admin/course-list/list`,
    method: 'get',
    params:data
  })
}
/**
 * 章节详情 接口地址 /plskadmin/api/admin/course-list/info
 * */
export function getCourseListInfoApi(data) {
  return request({
    url: `/admin/course-list/info`,
    method: 'get',
    params:data
  })
}
/**
 * 新增章节 接口地址 /plskadmin/api/admin/course-list/save
 * */
export function addCourseListApi(data) {
  return request({
    url: '/admin/course-list/save',
    method: 'POST',
    data
  })
}
/**
 * 修改章节 接口地址 /plskadmin/api/admin/course-list/update
 * */
export function updateCourseListApi(data) {
  return request({
    url: '/admin/course-list/update',
    method: 'POST',
    data
  })
}

/**
 * 接口地址 /plskadmin/api/admin/course-list/delete
 * */
export function deleteCourseListApi(data) {
  return request({
    url: '/admin/course-list/delete',
    method: 'DELETE',
    params:data,
  })
}
