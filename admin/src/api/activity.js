// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import request from '@/utils/request'

export function addActivityApi(data) {
  return request({
    url: '/admin/activity/save',
    method: 'POST',
    data
  })
}
/**
 * 活动列表
 * @param pram
 */
export function activityLstApi(params) {
  return request({
    url: '/admin/activity/list',
    method: 'GET',
    params
  })
}

/**
 * 删除活动
 * @param data
 */
export function deleteActivityApi(params){
  return request({
    url: '/admin/activity/delete',
    method: 'DELETE',
    params
  })
}
/**
 * 更新活动
 * @param data
 */
export  function updateActivityApi(data){
  return request({
    url: '/admin/activity/update',
    method: 'POST',
    data
  })
}

/**
 * 添加活动点位
 * @param data
 */
export function addLocationApi(data){
  return request({
    url: '/admin/activity-location/save',
    method: 'POST',
    data
  })
}

/**
 * 更新活动点位
 */
export function updateLocationApi(data){

}
/**
 * 删除活动点位
 * @param data
 */
export function deleteLocationApi(params){
  return request({
    url: '/admin/activity-location/delete',
    method: 'DELETE',
    params
  })
}

/**
 * 活动点位列表
 * @param params
 */
export function locationListApi(params){
  return request({
    url: '/admin/activity-location/list',
    method: 'GET',
    params
  })
}
/**
 * 活动签到记录列表
 * @param params
 */
export function activityLogListApi(params){
  return request({
    url: '/admin/activity-log/list',
    method: 'GET',
    params
  })
}
