// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import Layout from '@/layout'

const courseRouter = {
    path: '/course',
    component: Layout,
    redirect: '/course/index',
    name: 'Course',
    meta: {
      title: '课程',
      icon: 'clipboard'
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/course/index'),
        name: 'StoreIndex',
        meta: { title: '课程管理', icon: '' }
      },
      {
        path: 'detail/:courseId?',
        component: () => import('@/views/course/detail'),
        name: 'courseDetail',
        meta: { title: '课程详情', icon: '',noCache: true,
          activeMenu: `/course/index` }
      },
      {
        path: 'category',
        component: () => import('@/views/course/category/index'),
        name: 'Category',
        meta: { title: '课程分类', icon: '' }
      },
      {
        path: 'studycategory',
        component: () => import('@/views/course/studycategory/index'),
        name: 'Studycategory',
        meta: { title: '研学分类', icon: '' }
      }
      ,
      {
        path: 'integralcategory',
        component: () => import('@/views/course/integralcategory/index'),
        name: 'Integralcategory',
        meta: { title: '积分商品分类', icon: '' }
      },
      {
        path: 'integralBuy',
        component: () => import('@/views/marketing/integralBuy/index'),
        name: 'integralBuy',
        meta: { title: '积分商城', icon: '' },
        children: [
          {
            path: 'config',
            component: () => import('@/views/marketing/integralBuy/Config/index'),
            name: 'SeckillConfig',
            meta: { title: '积分商品配置', icon: '' }
          },
          {
            path: 'list/:timeId?',
            component: () => import('@/views/marketing/integralBuy/List/index'),
            name: 'integralBuyList',
            meta: { title: '积分商品', icon: '',noCache: true,
              activeMenu: `/marketing/integralBuy/list` }
          },
          {
            path: 'creatIntegralBuy/:name?/:timeId?/:id?',
            component: () => import('@/views/marketing/integralBuy/List/create'),
            name: 'CreatintegralBuy',
            meta: { title: '添加积分商品', icon: '', noCache: true,
              activeMenu: `/marketing/integralBuy/list` }
          }
        ]
      },

    ]
  }

export default courseRouter
