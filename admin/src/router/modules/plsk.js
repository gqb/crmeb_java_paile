import Layout from '@/layout'

const plskRouter = {
  path: '/plsk',
  component: Layout,
  redirect: '/plsk/index',
  name: 'PLSK',
  meta: {
    title: '派乐时刻',
    icon: 'clipboard'
  },
  children: [
    {
      path: 'index',
      component: () => import('@/views/plsk/index'),
      name: 'PlskIndex',
      meta: {title: '列表管理', icon: ''}
    },
  ]
}
export default plskRouter
