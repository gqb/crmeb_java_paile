// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import Layout from '@/layout'

const activityRouter = {
    path: '/activity',
    component: Layout,
    redirect: '/activity/index',
    name: 'Course',
    meta: {
      title: '活动',
      icon: 'clipboard'
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/activity/index'),
        name: 'ActivityIndex',
        meta: { title: '活动管理', icon: '' }
      }

    ]
  }

export default activityRouter
